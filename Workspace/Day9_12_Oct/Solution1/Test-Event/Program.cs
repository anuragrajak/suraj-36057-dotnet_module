﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using creatorDb;
namespace Test_Event
{//assuming dccreator is reffered
    class Program
    {
        static void Main(string[] args)
        {
            SqlServer dbObj = new SqlServer();

            mydelegate pointer1 = new mydelegate(OnInsertCallMe);
            mydelegate pointer2 = new mydelegate(OnUpdateCallMe);
            mydelegate pointer3 = new mydelegate(OnDeleteCallMe);

            dbObj.Inserted += pointer1;
            dbObj.Updated += pointer2;
            dbObj.Deleted += pointer3;

            dbObj.Insert("abcd");
            dbObj.Update("abcd");
            dbObj.Delete("abcd");

            Console.ReadLine();
        }
        public static void OnInsertCallMe()
        {
            Console.WriteLine("Logging Insert into Console");
        }

        public static void OnUpdateCallMe()
        {
            Console.WriteLine("Logging Update into Console");
        }

        public static void OnDeleteCallMe()
        {
            Console.WriteLine("Logging Delete into Console");
        }
    }
}
namespace creatorDb
{
    public delegate void mydelegate();


    public class SqlServer
    {
        public event mydelegate Inserted;
        public event mydelegate Updated;
        public event mydelegate Deleted;

        public void Insert(string data)
        {
            Console.WriteLine(data + "inserted into SQL Server");
            Inserted();
        }
        public void Update(string data)
        {
            Console.WriteLine(data + "Updated into SQL Server");
            Updated();
        }

        public void Delete(string data)
        {
            Console.WriteLine(data + "Deleted into SQL Server");
            Deleted();
        }
    }
       
}
    
    
