﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Consumer3
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter enter the dll/exe file path ");

            string pathOfAssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);

            Type[] allTypes = assembly.GetTypes();

            object classObject = null;

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type : " + type.Name);
                List<Attribute> allAttributes = type.GetCustomAttributes().ToList();

                bool isItSerializable = false;

                foreach (Attribute attribute in allAttributes)
                {
                    if(attribute is SerializableAttribute)
                    {
                        isItSerializable = true;

                        break;
                    }

                }

                if(isItSerializable)
                {
                    Console.WriteLine(type.Name + 
                        "is marked Serializable");

                } else
                {
                    Console.WriteLine(type.Name +
                       "is not marked Serializable");
                }

                classObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public |
                                                           BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.WriteLine("Calling " + method.Name + "  method ....");
                    ParameterInfo[] allParams = method.GetParameters();

                    object[] arguments = new object[allParams.Length];

                    for (int i = 0; i < allParams.Length; i++)
                    {
                        Console.WriteLine("Enter  " + allParams[i].ParameterType.ToString() +
                           "  value for " + allParams[i].Name  );

                        arguments[i] = Convert.ChangeType(Console.ReadLine(),allParams[i].ParameterType );


                    }

                    object result = type.InvokeMember(method.Name,
                              BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod,
                              null, classObject, arguments);

                    Console.WriteLine("Result of " + method.Name + "executed =  " + result.ToString() );

                }

            }
            Console.ReadLine();
        }
    }
}
