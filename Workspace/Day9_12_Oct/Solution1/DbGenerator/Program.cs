﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myDbCustomAttributes;
using System.Reflection;
namespace DbGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the path of your class library : \n");

            string path = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(path);

            Type[] allTypes = assembly.GetTypes();
            string query = ""; //

            foreach (Type type in allTypes)
            {
                List<Attribute> allAttributes = type.GetCustomAttributes().ToList();

                foreach (Attribute attribute in allAttributes)
                {
                    if(attribute is Table)
                    {
                        Table tableAttributeObject = (Table)attribute;

                        string tableName = tableAttributeObject.TableName;
                        query = query + "CREATE TABLE " + tableName + "(";
                        break;
                    }

                }

                PropertyInfo[] allGettersSetters = type.GetProperties();

                foreach (PropertyInfo propertyinfo in allGettersSetters)
                {
                    List<Attribute> allAttributesOnCurrentGetterSetter = propertyinfo.GetCustomAttributes().ToList();

                    foreach (Attribute attributeOnGetterSetter in allAttributesOnCurrentGetterSetter)

                    {
                        if(attributeOnGetterSetter is Column)
                        {
                            Column columnAttributeObject = (Column)attributeOnGetterSetter;

                            query = query + columnAttributeObject.ColumnName + " " + columnAttributeObject.ColumnType + ",";
                            break;
                        }


                    }

                }
                query = query.TrimEnd(new char[] { ',' });
                query = query + ");";



            }

            Console.WriteLine(query);
            Console.ReadLine();
        }
    }
}
