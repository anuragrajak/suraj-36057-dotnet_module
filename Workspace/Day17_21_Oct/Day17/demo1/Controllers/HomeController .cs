﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Mvc;
using demo1.Models;

namespace demo1.Controllers
{
    
    public class HomeController : BaseController
    {
        // GET: Home

        //DemoDBEntities dbObj = new DemoDBEntities();
        public ActionResult Index()
        {

           
                var emp = dbObj.Emps.ToList();

                ViewBag.MyMessage = "Employee list";

                ViewBag.UserName = User.Identity.Name; 
                return View(emp);
            

         
        }
        [AllowAnonymous]
        public ActionResult About()
        {
           
                ViewBag.MyMessage = "About Us";
                ViewBag.UserName = User.Identity.Name;
                return View();

           


         
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
           
                ViewBag.action = "/Home/Contact";
                ViewBag.method = "POST";
                ViewBag.UserName = User.Identity.Name;
                ViewBag.MyMessage = "Contact Us";

                ViewBag.success = "your query has been noted";
                return View();
           
          
        }
        [HttpPost]
        public ActionResult Contact(Contact contact)
        {

           
                ViewBag.done = contact.ToString();


                string email = ConfigurationManager.AppSettings["Email"];
                string password = ConfigurationManager.AppSettings["Password"];


                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(email);

                mail.To.Add(contact.Email);

                mail.CC.Add("surajporje69@gmail.com");

                mail.Subject = "New Query Recieved";

                mail.Body = "<h2>" + contact.ToString() + "</h2>";

                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential(email, password);

                smtp.EnableSsl = true;

                smtp.Send(mail);

                ViewBag.done = "Your Query submitted Successfully";

                return View();
           

           
        }




        [HttpGet]
        public ActionResult Edit(int id)
        {
           
                var empToModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                dbObj.SaveChanges();

                ViewBag.MyMessage = string.Format(" Update Employee {0} Details", empToModify.Name);
                return View(empToModify);
           
            
        }
        [HttpPost]
        public ActionResult Edit(Emp employee)
        {
           
                var empToModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == employee.No
                                   select emp).First();

                empToModify.Name = employee.Name;

                empToModify.Address = employee.Address;

                dbObj.SaveChanges();



                return Redirect("/Home/Index");
           


           
        }

        [HttpGet]
        public ActionResult Create()
        {
            
                ViewBag.MyMessage = string.Format(" Register New Employee");
                return View();

           

           
        }

        [HttpPost]
        public ActionResult Create(Emp employee)
        {
            
                dbObj.Emps.Add(new Emp()
                {
                    No = employee.No,
                    Name = employee.Name,
                    Address = employee.Address


                });



                dbObj.SaveChanges();



                return Redirect("/Home/Create");

           

           
        }


         public ActionResult Delete(int id)
        {
            
                var empToDelete = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                dbObj.Emps.Remove(empToDelete);


                dbObj.SaveChanges();


                return Redirect("/Home/Index");

           
          
        }


    }
}