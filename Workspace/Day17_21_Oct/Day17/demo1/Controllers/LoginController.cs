﻿using demo1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace demo1.Controllers
{
    public class LoginController : Controller
    {
        DemoDBEntities dbObj = new DemoDBEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn( HRLoginInfo loginObject,string ReturnUrl)
        {

            var MatchCount = (from hr in dbObj.HRLoginInfoes.ToList()
                               where hr.UserName.ToLower() == loginObject.UserName.ToLower()
                               && hr.Password == loginObject.Password
                               select hr).ToList().Count();

            if (MatchCount == 1)
            {
                // since user details are correct
                // create a session ... send cookie to cient 

                FormsAuthentication.SetAuthCookie(loginObject.UserName, false);


                //give instruction to shoot a call for '/Home/Index '

                if(ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");

                }




                return View();

            }
            else
            {
                ViewBag.ErrorMessage = "User Name or Password is incorrect";
                return View();
            }



            
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();

            return Redirect("/Home/About");
        }
    }
}