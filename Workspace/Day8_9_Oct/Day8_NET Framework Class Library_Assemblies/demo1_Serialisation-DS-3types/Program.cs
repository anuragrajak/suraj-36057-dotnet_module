﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace demo1_Serialisation_DS_3types
{
    class Program
    {
        static void Main(string[] args)
        {
            //FileStream

            // NetworkStream

            //CryptoStream

            #region Normal File Writing
            /* // FileStream fs = new FileStream("E:\\DAC\\claswork-bofore-git-commit\\Day7\\Data.txt")
             // declare stream with path,mode,access
             FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day7\Data.txt",
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);
             // need writer to write to file , for stream object fs 

             StreamWriter writer = new StreamWriter(fs);
             //use writer
             writer.WriteLine("hello world ");

             fs.Flush();  // forcefully write on harddrive if data is still in Ram not written due to small data size
             writer.Close();
             fs.Close(); // content in memory //ram still need to be written to file for small data written so use close 
 */
            #endregion

            #region Read content as string from a file.txt

            /* // declare stream with path,mode,access
             FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day7\Data.txt",
                                            FileMode.Open,
                                            FileAccess.Read);
             // need reader to read from  file , for stream object fs 
             StreamReader reader = new StreamReader(fs);
             //use reader
             string entireData = reader.ReadToEnd();

             *//*while (true)
             {
                 string entireData = reader.ReadLine();
                 if(entireData != null)
                 {
                     Console.WriteLine(entireData);
                 }
                 else
                 {
                      break;
                 }
             }*//*
             Console.WriteLine(entireData);

             reader.Close();
             fs.Close(); 
            */



            #endregion

            #region Object writing to File with Binary Serialization

            // only print class name, cant write class object to file as bit ,also known as serialization 

            /* Emp emp = new Emp();

             Console.WriteLine("please enter employeee no ");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("please enter employee name");
             emp.Name = Console.ReadLine();

             FileStream fs = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\demo1.txt",
                 FileMode.OpenOrCreate, FileAccess.Write);

             BinaryFormatter specialWriter = new BinaryFormatter();
             specialWriter.Serialize(fs, emp);


             specialWriter = null;

             fs.Flush();
             fs.Close();*/



            #endregion


            #region Object reading to File with Binary DeSerialization

            /* FileStream fs1 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\demo1.txt",
               FileMode.Open, FileAccess.Read);

             BinaryFormatter specialReader = new BinaryFormatter();
                object obj =   specialReader.Deserialize(fs1);

             if(obj is Emp)
             {
                 Emp e = (Emp)obj;
                 Console.WriteLine(e.getDetails());
             }

             specialReader = null;

             fs1.Close();*/



            #endregion


            #region book and emp serialize

            /*Emp e1 = new Emp();
             Book b1 = new Book();

             Console.WriteLine("Enter No:");
             e1.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             e1.Name = Console.ReadLine();


             Console.WriteLine("Enter ISBN :");
             b1.ISBN = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter book title");
             b1.Title = Console.ReadLine();


             ArrayList arr = new ArrayList();

             arr.Add(e1);
             arr.Add(b1);

             FileStream fs3 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\demo1.1.txt",
                                             FileMode.OpenOrCreate, FileAccess.Write);

             BinaryFormatter specialWriter3 = new BinaryFormatter();
             specialWriter3.Serialize(fs3, arr);


             specialWriter3 = null;

             fs3.Flush();
             fs3.Close();
 */
            #endregion


            #region book and emp deserialize
            /*FileStream fs4 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\demo1.1.txt",
                                             FileMode.Open, FileAccess.Read);

            BinaryFormatter specialReader3 = new BinaryFormatter();
               object obj = specialReader3.Deserialize(fs4);

              if(obj is ArrayList)
            {
                ArrayList arrayRead = (ArrayList)obj;
                foreach (Object o in arrayRead)
                { 

                    if(o is Emp)
                    {
                        Emp e = (Emp)o;
                        Console.WriteLine(e.getDetails());

                    } else if(o is Book)
                    {
                        Book b = (Book)o;
                        Console.WriteLine(b.Get_Book_Details());

                    } else
                    {
                        Console.WriteLine("Unkown Type ");
                    }

                }

            }
         

            fs4.Flush();
            fs4.Close();
*/
            #endregion

            #region XMl Serialization 
            /* Emp emp = new Emp();

             Console.WriteLine("Enter No:");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             emp.Name = Console.ReadLine();


             FileStream fs = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\Data.xml",
                                             FileMode.OpenOrCreate,
                                             FileAccess.Write);
             // in xml here, getter and setter, public method i.e public member get serialized  

             XmlSerializer writer = new XmlSerializer(typeof(Emp));

             writer.Serialize(fs, emp);

             writer = null;

             fs.Flush();

             fs.Close();*/
            #endregion

            #region XML deserialization




            /*FileStream fs = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\Data.xml",
                                            FileMode.Open,
                                            FileAccess.Read);
            // in xml here, getter and setter, public method i.e public member get serialized  
            XmlSerializer specialReader = new XmlSerializer(typeof(Emp));

            object obj = specialReader.Deserialize(fs);

            if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.getDetails());
            }
            else
            {
                Console.WriteLine("Unknown type of data !!");
            }

            specialReader = null;




            fs.Flush();

            fs.Close();*/

            #endregion

            #region Object writing to File with SOAP Serialization

            // only print class name, cant write class object to file as bit ,also known as serialization 
           /* Emp emp = new Emp();

            Console.WriteLine("Enter No:");
            emp.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            emp.Name = Console.ReadLine();


            FileStream fs = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\Data2.xml",
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);

            SoapFormatter writer = new SoapFormatter();

            writer.Serialize(fs, emp);

            writer = null;
            
            fs.Flush();
            fs.Close();
*/

            #endregion

            #region Object reading to File with SOAP DeSerialization

            // only print class name, cant write class object to file as bit ,also known as serialization 


            FileStream fs1 = new FileStream(@"E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\Data2.xml",
                                            FileMode.Open,
                                            FileAccess.Read);

            SoapFormatter specialReader = new SoapFormatter();

             object obj =  specialReader.Deserialize(fs1);

            if(obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine( e.getDetails());
            }
            else
            {
                Console.WriteLine("unknown");
            }

            specialReader = null;
            fs1.Flush();

            fs1.Close();

            #endregion



            Console.ReadLine();
        }
    }
    [Serializable]
    public class Emp
    {
      [NonSerialized]
        private string _Password = "suraj@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }
    }
    [Serializable]
    public class Book
    {
        private int _ISBN;

        private string _Title;




        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }



        public int ISBN
        {
            get { return _ISBN; }
            set { _ISBN = value; }
        }

        public string Get_Book_Details()
        {
            return "ISBN = " + " " + this.ISBN.ToString() + "book title = " + this.Title + "\n";
        }


    }
}