﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db; //Dll refered into customer project
using MyLoggerLib;
namespace Test_EventsDelegates
{
    class Program
    {// code witten in Db 
        // purchased dll written here 
        
        static void Main(string[] args)
        {
            SQLServer dbobj = new SQLServer();

            mydelegate pointer1 = new mydelegate(OnInsertCallMe);
            mydelegate pointer2 = new mydelegate(OnUpdateCallMe);
            mydelegate pointer3 = new mydelegate(OnDeletedCallMe);

            dbobj.Inserted += pointer1;

            dbobj.Updated += pointer2;

            dbobj.Deleted += pointer3;


            dbobj.Insert("abcd");
            dbobj.Update("abcd");
            dbobj.Delete("abcd");
            Console.ReadLine();
        }
        public static void OnInsertCallMe()
        {
            Console.WriteLine("  Inserted into SQL Server");

        }
        public static void OnUpdateCallMe()
        {
            Console.WriteLine("Updated into SQL Server");

        }
        public static void OnDeletedCallMe()
        {
            Console.WriteLine(" Deleted into SQL Server");

        }
    }
}

namespace  Db
{
    public delegate void mydelegate();

    public class SQLServer
    {
         public event  mydelegate  Inserted;
         public event  mydelegate Updated;
         public event  mydelegate Deleted;
        public void Insert(string data)
        {
              Console.WriteLine(data + "  Inserted into SQL Server");
           //raise Event just like raising trigger in database
            
          //  Logger.CurrentLogger.Log(data + "  Inserted into SQL Server");
            Inserted();
        }
        public void Update(string data)
        {
             Console.WriteLine(data + "   Updated into SQL Server");
            Updated();
           // Logger.CurrentLogger.Log(data + "   Updated into SQL Server");
        }
        public void Delete(string data)
        {
           Console.WriteLine(data + "  Deleted into SQL Server");
            Deleted();
            //Logger.CurrentLogger.Log(data + "  Deleted into SQL Server");
        }
       
    }



    
}