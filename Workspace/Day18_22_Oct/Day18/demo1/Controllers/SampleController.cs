﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using demo1.Models;
namespace demo1.Controllers
{
    public class SampleController : BaseController
    {
        // GET: Sample
        public ActionResult Index()
        {


            var emp = dbObj.Emps.ToList();

            ViewBag.MyMessage = "Employee list";

            ViewBag.UserName = User.Identity.Name;
            return View(emp);



        }
    


        [HttpGet]
        public ActionResult Edit(int id)
        {

            var empToModify = (from emp in dbObj.Emps.ToList()
                               where emp.No == id
                               select emp).First();

            dbObj.SaveChanges();

            ViewBag.MyMessage = string.Format(" Update Employee {0} Details", empToModify.Name);
            return View(empToModify);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Emp employee)
        {

            var empToModify = (from emp in dbObj.Emps.ToList()
                               where emp.No == employee.No
                               select emp).First();

            empToModify.Name = employee.Name;

            empToModify.Address = employee.Address;

            dbObj.SaveChanges();



            return Redirect("/Sample/Index");




        }

        [HttpGet]
        public ActionResult Create()
        {

            ViewBag.MyMessage = string.Format(" Register New Employee");
            return View();




        }

        [HttpPost]
        public ActionResult Create(Emp employee)
        {

           
             if (ModelState.IsValid)
            {
               /* dbObj.Emps.Add(new Emp()
                {
                    No = employee.No,
                    Name = employee.Name,
                    Address = employee.Address


                });*/

                dbObj.Emps.Add(employee);
                dbObj.SaveChanges();
                return Redirect("/Sample/Create");
            }
             else
            {
                 return View(employee);
            }


           







        }


        public ActionResult Delete(int id)
        {

            var empToDelete = (from emp in dbObj.Emps.ToList()
                               where emp.No == id
                               select emp).First();

            dbObj.Emps.Remove(empToDelete);


            dbObj.SaveChanges();


            return Redirect("/Sample/Index");



        }


        public JsonResult Validate(int id)
        {
            int Count = (from emp in dbObj.Emps.ToList()
                               where emp.No == id
                               select emp).Count();

            return new JsonResult() { Data = Count, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

    }
}