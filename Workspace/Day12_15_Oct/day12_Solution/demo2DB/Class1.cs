﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2DB
{
    class DbOperations
    {
        public List<string> SelectOperation()
        {
            List<string> selected = new List<string>();
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                               Integrated Security=True;Pooling=False");
                con.Open();

                SqlCommand cmd = new SqlCommand("select * from Emp", con);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    /*  Console.WriteLine(reader[0]);
                           Console.WriteLine(reader[1]);
                    Console.WriteLine(reader[2]); */

                    string data = string.Format("emp No = {0}, emp name = {1}, emp Address = {2}",
                           reader["Id"], reader["Name"], reader["Address"]);

                    selected.Add(data);
                }

               
            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format("error message = {0}",ex.Message));
            }
            finally
            {
                con.Close();
            }

            return selected;
        }


        public void InsertOperation()
        {

            SqlConnection con = null;

            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                    Integrated Security=True;Pooling=False");

                con.Open();

                Console.WriteLine("Enter employee no");
                int no = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ente remployee Name");
                string Name = Console.ReadLine();
                Console.WriteLine("Ente remployee Address");
                string Address = Console.ReadLine();

                string query = "insert into Emp Values({0},'{1}','{2}')";

                string finalQuery = string.Format(query,no, Name,Address);

                SqlCommand cmd = new SqlCommand(finalQuery,con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update /delete


                Console.WriteLine(string.Format("no of rows affected = {0}", noOfRowsAffected.ToString()));

            }
            catch (Exception ex )
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }
            finally
            {
                con.Close();
            }

           
        }

        public void UpdateOperation()
        {

            SqlConnection con = null;

            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                    Integrated Security=True;Pooling=False");

                con.Open();

                Console.WriteLine("Enter employee no");
                int no = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ente remployee Name");
                string Name = Console.ReadLine();
                Console.WriteLine("Ente remployee Address");
                string Address = Console.ReadLine();

                string updateQuery = "update Emp SET Name = '{1}',Address = '{2}' where Id = {0}";
                

                string finalQuery = string.Format(updateQuery, no, Name, Address);

                SqlCommand cmd = new SqlCommand(finalQuery, con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update /delete


                Console.WriteLine(string.Format("no of rows affected = {0}", noOfRowsAffected.ToString()));

            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }
            finally
            {
                con.Close();
            }


        }

        public void DeleteOperation()
        {

            SqlConnection con = null;

            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                    Integrated Security=True;Pooling=False");

                con.Open();

                Console.WriteLine("Enter employee no");
                int no = Convert.ToInt32(Console.ReadLine());

               

                string deleteQuery = "delete from Emp WHERE Id = {0}";


                string finalQuery = string.Format(deleteQuery, no);

                SqlCommand cmd = new SqlCommand(finalQuery, con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update /delete


                Console.WriteLine(string.Format("no of rows affected = {0}", noOfRowsAffected.ToString()));

            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }
            finally
            {
                con.Close();
            }


        }



    }
}
