﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{//developer 1
   public partial class Emp
    {
        private int _Age;

       partial void  Validate(string propertyName, object Value);
        public int Age
        {
            get { return _Age; }


            set 
            {
                Validate("Age", value);
                _Age = value;
            }
        }

    }
}
