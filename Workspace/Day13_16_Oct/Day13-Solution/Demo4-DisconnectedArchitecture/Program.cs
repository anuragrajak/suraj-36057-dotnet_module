﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Demo4_DisconnectedArchitecture
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Fetch Data using Disconnected Architecture
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=DemoDB;Integrated Security=True;Pooling=False");

            SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from Emp", con);

            dataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

            dataAdapter.Fill(ds, "abc");
            #endregion


            #region Insert using Disconnected Architecture
            /* DataRow newRow = ds.Tables["abc"].NewRow();

             newRow["No"] = 30;
             newRow["Name"] = "Harish";
             newRow["Address"] = "goa";

             ds.Tables[0].Rows.Add(newRow);

             dataAdapter.Update(ds, "abc");*/
            #endregion

            #region Update using Disconnected Architcture
            /*Console.WriteLine("Enter a Emp No for update");
            int no = Convert.ToInt32(Console.ReadLine());

            DataRow rowToModify = ds.Tables[0].Rows.Find(no);

            if (rowToModify != null)
            {
                Console.WriteLine("Record Found");

                Console.WriteLine("Current Name = " + rowToModify["Name"].ToString());
                Console.WriteLine("Current Address = " + rowToModify["Address"].ToString());
                Console.WriteLine("------------------------------------------");

                Console.WriteLine("Please Enter New Name");
                rowToModify["Name"] = Console.ReadLine();

                Console.WriteLine("Please Enter New Address");
                rowToModify["Address"] = Console.ReadLine();

            }
            else
            {
                Console.WriteLine("Sorry! No record Found");
            }

            dataAdapter.Update(ds, "abc");*/
            #endregion

            #region Delete using Disconnected Architecture
            /* Console.WriteLine("Please enter Emp No to be deleted");
            int no2 = Convert.ToInt32(Console.ReadLine());

            DataRow rowToDelete = ds.Tables[0].Rows.Find(no2);

            if (rowToDelete != null)
            {
                rowToDelete.Delete();
            }
            else
            {
                Console.WriteLine("Sorry! No record Found");
            }

            dataAdapter.Update(ds, "abc");*/
            #endregion


            Console.ReadLine();



        }
    }
}
