﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo2.PlainOldClrObject;
using Demo2.DataAccessLayer;
using System.Configuration;
using System.Data.SqlClient;

namespace Demo2.DataAccessLayer
{
     public class SQLServer:IDatabase
    {
       public List<Emp> Select()
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
                List<Emp> emplist = new List<Emp>();
                


                string query = "select * from Emp";

                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    Emp emp = new Emp()
                    {
                        No = Convert.ToInt32(reader["No"]),
                        Name = reader["Name"].ToString(),
                        Address = reader["Address"].ToString()
                    };

                    emplist.Add(emp);
                }

                return emplist;

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return null;
            }
            finally
            {
                con.Close();
            }


           
           
        }

        public int Insert(Emp emp)
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
               

                string query = "insert into Emp values({0},'{1}','{2}')";
                string finalQuery = string.Format(query, emp.No, emp.Name, emp.Address);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }

          

           

           
            
        }

        public int Update(Emp emp)
        {

            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {

                string query = "update Emp Set Name = '{0}',Address = '{1}' where  No = {2}";

                string finalQuery = string.Format(query, emp.Name, emp.Address, emp.No);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        public int Delete(Emp emp)
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {

                string query = "delete from Emp where  No = {0}";

                string finalQuery = string.Format(query, emp.No);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
