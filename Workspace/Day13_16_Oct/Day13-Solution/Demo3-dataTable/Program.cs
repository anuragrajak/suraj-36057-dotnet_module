﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Demo3_dataTable
{
    class Program
    {
      static void Main(string[] args)
      {

            #region Data table concept
            /* DataTable dt = new DataTable("myTable");

             DataColumn c1 = new DataColumn("No", typeof(int));

             DataColumn c2 = new DataColumn("Name", typeof(string));
             DataColumn c3 = new DataColumn("Address", typeof(string));

             dt.Columns.Add(c1);
             dt.Columns.Add(c2);
             dt.Columns.Add(c3);

             dt.PrimaryKey = new DataColumn[] { c1 };

             DataRow r1 = dt.NewRow();
             r1["No"] = 21;
             r1["Name"] = "Chris";
             r1["Address"] = "Nagpur";

             DataRow r2 = dt.NewRow();
             r2["No"] = 22;
             r2["Name"] = "rice";
             r2["Address"] = "Kanpur";

             DataRow r3 = dt.NewRow();
             r3["No"] = 23;
             r3["Name"] = "cansas";
             r3["Address"] = "igatpuri";

             dt.Rows.Add(r1);
             dt.Rows.Add(r2);
             dt.Rows.Add(r3);*/


            #endregion

            #region using Dataset concept 
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("myTable");

            DataColumn c1 = new DataColumn("No", typeof(int));

            DataColumn c2 = new DataColumn("Name", typeof(string));
            DataColumn c3 = new DataColumn("Address", typeof(string));

            dt.Columns.Add(c1);
            dt.Columns.Add(c2);
            dt.Columns.Add(c3);

            dt.PrimaryKey = new DataColumn[] { c1 };
            //--------------------------------

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=DemoDB;Integrated Security=True;Pooling=False");

            SqlCommand cmd = new SqlCommand("select * from Emp", con);

            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DataRow row = dt.NewRow();
                row["No"] = Convert.ToInt32(reader["No"]);

                row["Name"] = reader["Name"];

                row["address"] = reader["Address"];

                dt.Rows.Add(row);

            }

            con.Close();

            //--------------------------------
            ds.Tables.Add(dt);

            //---------------------------------

            DataRow newRow = ds.Tables[0].NewRow();

            newRow["No"] = 40;

            newRow["Name"] = "milkha";
            newRow["Address"] = "Lahore";

            ds.Tables[0].Rows.Add(newRow);

            //------------Book Table---------------------

            DataTable book = new DataTable("Book");

            DataColumn bc1 = new DataColumn("ISBN", typeof(int));

            DataColumn bc2 = new DataColumn("Title", typeof(string));

            book.Columns.Add(bc1);
            book.Columns.Add(bc2);

            DataRow brow = book.NewRow();

            brow["ISBN"] = 100;
            brow["Title"] = "Let us C#";

            book.Rows.Add(brow);

            //---------------------------------------
            ds.Tables.Add(book);
            //----------------------------------------
            #endregion



            Console.ReadLine();

        }
    }
}
