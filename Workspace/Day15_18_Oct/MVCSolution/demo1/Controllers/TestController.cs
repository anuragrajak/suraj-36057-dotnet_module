﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using demo1.Models;
namespace demo1.Controllers
{
    public class TestController : Controller
    {
       DemoDBEntities1 dbObj = new DemoDBEntities1();
        // GET: Test
        public ActionResult Show()
        {

            try
            {
                var allEmps = dbObj.Emps.ToList();

                return View("", allEmps);
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }


            #region using List of Emp
            /* List<Emp> allEmps = new List<Emp>()
                      {              new Emp() { No = 1, Name = "suraj", Address = "nasik" },
                                     new Emp() { No = 1, Name = "raj", Address = "nasik" },
                                     new Emp() { No = 2, Name = "sur", Address = "nasik" },
                                      new Emp() { No = 3, Name = "saj", Address = "nasik" },
                                      new Emp() { No = 4, Name = "suj", Address = "nasik" }
                      };
*/
            #endregion

           
           
        }

       
        // GET: Test/Create
        public ActionResult Create()
        {



            return View();
        }

        // POST: Test/Create
       
        public ActionResult AfterCreate(FormCollection collection)
        {
            try
            {
                dbObj.Emps.Add(new Emp()
                {
                    No = Convert.ToInt32(collection["No"]),
                    Name = collection["Name"].ToString(),
                    Address = collection["Address"].ToString()
                }
            );

                dbObj.SaveChanges();

                return Redirect("/Test/Create");
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }




           
        }

        // GET: Test/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var EmpToModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                return View(EmpToModify);
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }

           
        }

        public ActionResult AfterEdit(FormCollection entireForm)

        {

            try
            {

                int empNo = Convert.ToInt32(entireForm["No"]);

                var empTOModify = (from emp in dbObj.Emps.ToList()
                                   where emp.No == empNo
                                   select emp).First();

                empTOModify.Name = entireForm["Name"].ToString();

                empTOModify.Address = entireForm["Address"].ToString();

                dbObj.SaveChanges();

                return Redirect("/Test/Show");

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }





        }

        // GET: Test/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {

                var EmpTODelete = (from emp in dbObj.Emps.ToList()
                                   where emp.No == id
                                   select emp).First();

                dbObj.Emps.Remove(EmpTODelete);

                dbObj.SaveChanges();

                return Redirect("/Test/Show");

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }


           
        }

        
    
    }
}
