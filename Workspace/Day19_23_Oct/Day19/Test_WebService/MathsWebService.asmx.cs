﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Test_WebService
{
   /* /// <summary>
    /// Summary description for MathsWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]*/

    [WebService]
    public class MathsWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public int Add(int x, int y)
        {
                return x + y;
         }

        [WebMethod]
        public int Sub(int x, int y)
        {
            return x - y;
        }

    }
}
