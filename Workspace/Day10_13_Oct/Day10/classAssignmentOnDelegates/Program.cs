﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classAssignmentOnDelegates
{
    delegate string mystring();


    class Program
    {
        static void Main(string[] args)
        {
            //You can use this main method for solution of the problem statement

            /* B class1 = new B();

             A class2 = new A();

             mystring pointer1 = new mystring(class1.M2);

             class2.M1(pointer1); */

            A aObj = new A();
            B bobj = new B();

            mystring pointer = new mystring(bobj.M2);

            aObj.M1(pointer);

            Console.ReadLine();
        }
    }

    public class A
    {
        //You can pass one parameter to M1
        //but parameter shoud not be of type string
        //parameter shoud not be of type  B 
        //parameter shoud not be of type Object
        public void M1(mystring p1)

        {   // Here you will have to call M2 method from B class
            //and print here what M2 returns on the screen..

            // M2();

            Console.WriteLine(p1());



            // but conditions are: Here in this code of M1
            // 1. You wil not declare B Object
            // 2. No usage of Generics / Inheritance / Overloading  OverRiding 
            // 3. No Usage of Static / abstract / singletone
            // 4. No Usage of Collections
            // 5. No Events
            // 6. No File Io / Serialization
            // 7. No Partial Class, Nullbale Type, Anonymous Method, Lambada Expression to be used 
            // 8. No DLL Concept

        }
    }

    public class B
    {
        public string M2()
        {
            return "M2 from  B";
        }
    }
}

