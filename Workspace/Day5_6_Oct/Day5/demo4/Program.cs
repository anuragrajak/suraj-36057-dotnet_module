﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Logger log = new Logger();
            try
            {
                Logger.CurrentLogger.Log("first log");
                Logger.CurrentLogger.Log("second Log");


                Employee emp = new Employee();
                emp.Emplyee_Detail();

                Maths m1 = new Maths();
                Console.WriteLine("Addition of two numbers = " +  m1.Add(10, 20));
                


                Console.ReadLine();
            }
            catch (Exception ex)
            {

                Console.WriteLine("error message " + ex.Message);
                Console.ReadLine();
            }
           

        }
    }

    public class Logger
    {

        private static Logger _logger = new Logger();

        private Logger()
        {
            Console.WriteLine("Logger Object Created .... ");

        }

        public static Logger CurrentLogger
        {
            get { return _logger; }
        }

        public void Log(string msg)
        {
            Console.WriteLine("logger " 
                               + msg 
                               + " "
                               + "@"
                               + DateTime.Now.ToString());
        }

    }
    public class Employee
    {
        public void Emplyee_Detail()
        {
            Logger.CurrentLogger.Log(" employeee details ");
        }
    }

    public class Maths
    {
        public int Add(int x,int y)
        {
            Logger.CurrentLogger.Log("Addition function done");
            return x + y;
        }
    }

}
