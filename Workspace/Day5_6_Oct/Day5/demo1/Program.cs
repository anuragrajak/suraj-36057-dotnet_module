﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Below is the way to write Getter Setter like C++
            Person p1 = new Person();
            p1.Set_Name("Suraj");
            p1.Set_Age(26);

            Console.WriteLine(p1.Get_Name());
            Console.WriteLine(p1.Get_Age());

            p1.Person_Detail();

            Person p2 = new Person("sagar", 23);

            p2.Person_Detail();



            Console.ReadLine();
            #endregion




        }
    }

    public class  Person
    {
        #region Data members

      private string _Name;
      private int _Age;
        #endregion


        #region constructor
        public Person()
        {
            this._Name = "";
            this._Age = 0;
        }

        public Person(string name,int age)
        {
            this._Name = name;
            this._Age = age;
        }

        #endregion

        #region Getter and Setter

        /// <summary>
        /// This function helps you set up Name of the person.
        /// you can try setting the name using personObject.Set_name("suraj");
        /// </summary>
        /// <param name="Name">this is nameof a person</param>

        public void Set_Name(string Name)
        {
            if(Name != "")
            {
                this._Name = Name;
            }
            else
            {
                this._Name = "No Data";
            }
           
        }

        public void Set_Age(int Age)
        {
            this._Age = Age;
        }

        public string Get_Name()
        {
            return this._Name;
        }
        public int Get_Age()
        {
            return this._Age;
        }
        #endregion

        #region member function

        public void Person_Detail()
        {
            Console.WriteLine("Person Name = " + this.Get_Name() + "Person Age = " + this.Get_Age());
        }

        #endregion
    }
}
