﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Employee Object using throughout the demo
           /*
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "suraj";


            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "suraj";


            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "suraj";
           */
            #endregion

            #region simple type casting
            /* 
             int i = 100;
              //Object obj = new Object();
           object obj = i;
             obj = "abcd";
             Emp e = new Emp();

             obj = e;
             if (obj is int)
             {
                 int j = Convert.ToInt32(obj);
                 Console.WriteLine(j);
             }
             else if (obj is string)
             {
                 string s = Convert.ToString(obj);
                 Console.WriteLine(obj);
             }
             else if (obj is Emp)
             {
                 e1 = (Emp)obj;
                 // Emp e2 = obj as Emp;

                 Console.WriteLine(e1.EmpDetails());
             }
             */

            #endregion

            #region simple integer array - I
            /*
            int[] arr = new int[3];
            
            arr[0] = 100;
            arr[1] = 200;
            arr[2] = 300;
            // arr[3] = 400;
            
            for (int i = 0; i < arr.Length; i++)
            {
            
                Console.WriteLine(arr[i]);
            }
            */
            #endregion

            #region simple integer Array - II
            /*
             int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

             for (int j = 0; i < arr.Length; i++)
             {

                 Console.WriteLine(arr[j]);
             }
            */
            #endregion

            #region Emp Array
            /*
           
             Emp[] employees = new Emp[3];
            
             employees[0] = e1;
             employees[1] = e2;
             employees[2] = e3;
            
             for (int i = 0; i < employees.Length; i++)
             {
                 Emp e = employees[i];
                 Console.WriteLine(e.EmpDetails());
            
             }
            */
            #endregion

            #region  Object Array of Fixed size
            /*
              // Console.WriteLine(e1); // here it prints namespace name. class name

            object[] arr = new object[5];
            arr[0] = e1; 
            arr[1] = 100; // Boxing
            arr[2] = "abcd";
            arr[3] = e2;
            arr[4] = false; //Boxing - to heap from stack


            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] is int)
                {
                    int k = Convert.ToInt32(arr[i]); //unboxing
                    Console.WriteLine(k);
                } else if(arr[i] is string)
                {
                    string s = Convert.ToString(arr[i]);
                    Console.WriteLine(s);
                } else if(arr[i] is Emp)
                {
                    Emp e = (Emp)arr[i];
                    Console.WriteLine(e.EmpDetails());
                } else if(arr[i] is bool)
                {
                    bool b = Convert.ToBoolean(arr[i]); //unboxing 
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type used");
                }

            }
            */
            #endregion

            #region ArrayList: Array of object
           /*
            ArrayList arr = new ArrayList();
            arr.Add(e1);
            arr.Add(100); // Boxing
            arr.Add("abcd");
            arr.Add(e2);
            arr.Add(false); //Boxing - to heap from stack
            arr.Add(DateTime.Now.ToString());
            arr.Add(new int[] { 10, 20, 30 });
            for (int i = 0; i < arr.Count; i++)
            {
                
                if (arr[i] is int)
                {
                    int k = Convert.ToInt32(arr[i]); //unboxing
                    Console.WriteLine(k);
                }
                else if (arr[i] is string)
                {
                    string s = Convert.ToString(arr[i]);
                    Console.WriteLine(s);
                }
                else if (arr[i] is Emp)
                {
                    Emp e = (Emp)arr[i];
                    Console.WriteLine(e.EmpDetails());
                }
                else if (arr[i] is bool)
                {
                    bool b = Convert.ToBoolean(arr[i]); //unboxing 
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type used");
                }

            }

            */
            #endregion

            #region declare and add Hashtable 
           /*
            Hashtable arr = new Hashtable();
            arr.Add("a", e1);

            arr.Add("b", 100); // Boxing
            arr.Add("c", "abcd");
            arr.Add("d", e2);
            arr.Add("e", false); //Boxing - to heap from stack
            arr.Add("f", DateTime.Now.ToString());
            arr.Add("g", new int[] { 10, 20, 30 });
            */
            #endregion


            #region  use string key to find a value
           /*
            Console.WriteLine("what would you like to enter as a key ");
            string key = Console.ReadLine();

            object obj = arr[key];
            if (obj is int)
            {
                int k = Convert.ToInt32(obj); //unboxing
                Console.WriteLine(k);
            }
            else if (obj is string)
            {
                string s = Convert.ToString(obj);
                Console.WriteLine(s);
            }
            else if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.EmpDetails());
            }
            else if (obj is bool)
            {
                bool b = Convert.ToBoolean(obj); //unboxing 
                Console.WriteLine(b);
            }
            else
            {
                Console.WriteLine("unknown type used");
            }
             */
            #endregion


            #region hash-table: key-value pair collection,key value ar eobjects,no limit 
            /*
            foreach (object keys in arr.Keys)
            {
                Console.WriteLine(keys);
                object obj = arr[keys];
                if (obj is int)
                {
                    int k = Convert.ToInt32(obj); //unboxing
                    Console.WriteLine(k);
                }
                else if (obj is string)
                {
                    string s = Convert.ToString(obj);
                    Console.WriteLine(s);
                }
                else if (obj is Emp)
                {
                    Emp e = (Emp)obj;
                    Console.WriteLine(e.EmpDetails());
                }
                else if (obj is bool)
                {
                    bool b = Convert.ToBoolean(obj); //unboxing 
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type used");
                }

            }
            */
            #endregion



            Console.ReadLine();
        }
    }

    public class Emp
    {


        private string _Name;

        private int _No;

        public Emp()
        {
            this.Name = "suraj";
            this.No = 1;
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }



        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string EmpDetails()
        {
            return "emp name" + this.Name.ToString() + " emp no" + this.No.ToString();
        }

    }
}
