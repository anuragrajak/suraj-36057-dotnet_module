# .NET stands for Domain NETwork -- Domain for (.)
- Syllabus
- Introduction to NET 4.5 Frameworks: Application Domain,Language Interoperability,.NET Framework Class Library,Assemblies,Introduction of Windows Presentation Foundation,Introduction of Windows Communication Foundation.

- C# .NET 4.5: Need of C#,Operators,Namespaces & Assemblies,Arrays,Preprocessors,Delegates and Events,Boxing and Unboxing,Regular Expression,Collections ,Exceptions Handling,Introduction to win forms.

- ASP .NET 4.5: Building .NET components,ADO.NET 4.5,Querying with LINQ,Custom Control,Master Pages, Themes and skins,Introduction to Web Services,MS.NET MVC Framework,Enterprise Services,Personalization and Localization,Deployment


### .Net development process(step 1 to 3 are developer step) (step 4 onwards client)

1. code in .NET supported Language [85 + language supported ,official and unofficial {
    C++,C#,VB.NET,f#,j#,Phyton,Ruby... officially 32 + , 
} ]
                      
2. Language Specific Compiler --- Additional Logic(C#,VB,F#)
3. MSIL (Microsoft Intermediate Language) - similar to 8085/8086 (EXE)
4. EXE recieved by client containing MSIL code  
  -  Call to OS
   - (Call to Common Language Runtime)
   - converts MSIL code into Binary specific to clients cpu
   - EXEcution of binary code
### Developer Requirement
2. Windows OS
1. Visual Studio --> 
  -  + VB.NET
     + C#
     + F#
     + VC ++
     + IRON
     + Python



### Client requirement 
  1. Window OS
  2. .NET Framework (FREE and Redistributable )
  - this gives (Call to common Language runtime) (CLR)  


### C++ development process(step 1 to 3 are developer step) (step 4 onwards client)

1. C++ Code
2. C++ Compiler
3. Binary Code (on windows EXE ) ----> 
4. given to client EXE -   containing Binary
 -  Code Call to OS 
  - Allocate memory
  - EXEcution of the binary code

 <img src="compare .NEt and C++.jpg" alt="|">
 <hr>


 ### Developer Machine

 1. Windows OS
 2. Visual Studio 2019
     - Profession  Edition
     - Enterprise Edition
     - Community  Edition
        - free And Downloadable from MS website

### To Learn
- console application ,language C#
- web, language ASP.NET 

### .NET advantage  

 - 

### .NET Framework 
- open source , source code avaialable on github
 - 2 flavours
  1. **.NET (use ths for course)**
  -  Only on Windows Platform (client side) development can be done on
  > 1. Visual Studio 2019 Community Edition
    (prefered  on windows)

  - 1. Console Application
    2. Windows Application
    3. Web Application  
  2. **.NET Core (demo at end on module)**
  - for Windows/Mac/Linux(client side) ,development can be done on  
   > 1. Visual Studio 2019 Community Edition
    (prefered  on windows)
   
   > 2. Visual Studio Code (preferred on MAC/Linux)


### code conversion in .NET
1. **C# myCode.cs**
  ```c#
  int i = 100;
  ```
2. --> C sharp Compiler( CSC.EXE)  <---> **Common Type  System**
3. --> MSIL (Micsrosoft Intermediate Language) gives
```
System.Int32 i = 100

```

2. **VB.NET Language mycode.vb**

```vb
dim i as integer
i = 100

```
2. --> Visual Basic Compiler ( VBC.EXE) <---> **Common Type System**
3. --> MSIL (Micsrosoft Intermediate Language) gives
```
  System.Int32 i = 100
```

- **Common Type System**
- to convert any language to MSIL 
 - it is table with two column 
  1. General Type 
  - e.g 
    - int
    - short
    - long 

  2. MSIL Representation (for General Type)
  - e.g 
    - system.int32
    - system.int16
    - system.int64

| no |  General Type |  MSIL Representation |  
|---|---|---|---|---|
|  1 | int  | system.int32  |   
|  2 | short  | system.int16  |   
|   3| long   |  system.int64 |
|


### .NET Output is in Assembly code,it can have --> (Extension : EXE/DLL)
   - DLL : Dynamic Linked Library
   - EXE : EXEcutable file
1. it has 4 major layer : (it makes Assembly)
   - 1. Assembly Meta data
   - 2. Resources
   - 3. Type Metadata
   - 4. MSIL
2. so output is given as Assembly   


<img src="language conversion.jpg">
<hr>


### Flow of .NET (how it works) through CLR(Common Language Runtime)

#### Requirement for .NET working 
1. on developer machine

|Developer Machine|
|-----------------------------------| 
|Visual Studio                      |
| + Language Support|
| + .NET Framework|
| + Language Compiler|
| + CLR (Common Language Rountime)|

2. On client Machine (windows OS)
 - .NET Framewok (CLR) 

### Flow  for .NET

1. Language Code
2. Language Compiler <--- CTS (Common Type System)
3. MSIL
4. Assembly

| Assembly:sample .EXE|    
|------------------|
| Assembly MetaData|
| Resource  (references to external file )       |
| Type MetaData    |
| MSIL             |
|------------------|


5. Entry Point at Assembly
- Call to CLR(Common Language Runtime)
-  1. Call to OS
   2. OS will look at Entry Point Looks 
    for instruction (if any)
   3. Instruction : Load CLR
     - Try to find CLR installation on the machine
        + you need .NET Framework Installation for having CLR
   4. CLR is in Memory   
   5. CLR Reads your EXE 
   (step by Step - Top to Bottom)
   -   1. **Reads Assembly Metadata**
        - just like index page of any book  
       2. **Read and Load Rescources**
       - e.g it means loading dependencies if any 
       3. **Type Data**
        - Extra information about the types
        - e.g. 
         - classes/ structures will be read and acted as needed 
        - **for FIle IO part we will learn about it** 
       4. **give MSIL to CLR**
       > ->  Bring "JIT Compiler in memory
         -> Give JIT Compiler to MSIL COde
         -> Expect "Binary Code" in return   
    6. CLR will feed this Binary Code to CPU --> CPU                

- so we have communication in the following way

| OS |<---> |CLR |<---> |application|
|----|------|----|------|-----------|

- so



<img src="flow of CLR on Assembly.jpg" >

<hr>


### DLL 
- CLR stands for **Common Language Runtime**
 - memory allocated by CLR for application (Myapp.EXE)
   - so any EXEcution of app, i.e DLL (Math.DLL)
   will have to be allocated memory from Myapp.EXE memory space 
   - so it(DLL) is called as a **Parasite**  , i.e header files in compiled format 
   - two types
   - 1. DLL - trusted 
     2. Special DLL -untrusted
   - all reusable code is kept in DLL 

- its ok when DLL is from trusted source
- what if DLL is not from a trusted source ?

  - use surrogate EXE (as a protection )
  - so our app.EXE space doesnt get corrupted 

  <hr>
<img src="typesOfDLL.jpg" alt="special DLL">
  <hr>
 
```
                         DDL (Dynamc Linked Library)
                         - Math.DLL
                         - DBCode.DLL
 .NET Code     <------ - FileIO.DLL
{
  your code  
}    ---->             Assembly (EXE)
                       + Math.DLL
                       + DBCode.DLL
                       + FileIO.DLL  
                      (Myapp.EXE)

```

# day 2

### Types of DLL 
1. **InProc DLL** 
 - dll libraries are server application
 - EXE application are client side application
     -running EXE is called client process
     - server .dll run within process of client is called 
     **in  process Server or InProc DLL**
     - i.e server get memory in EXE memory space

2. **Out Proc DLL**
- in this, we ger a surrogate EXE like Host.EXE for carrying 
out the EXEcution , so in this case 
  dll memory get allocated in host.EXE , so 
  as memory get allocated outside the EXE, it is called 
  out Proc Server or out Proc DLL

<img src="type of Dll.jpg">
<hr>

3. **MSCORLIB.DLL (when we  LOad CLR, we get this DLL)**
- stands for <u>M</u>icrosoft <u>C</u>ommon <u>O</u>bject <u>R</u>untime <u>L</u>ibrary
- it is a Out proc DLL
- CLR demands memory from CPU to be managed, 
- here it loads all Out-Proc DLL, and its related EXE
- single CLR can manage many EXE, CLR is loaded only once 
- every EXE loaded, have its **Application Domain** in Host Process space of CLR, it is a Virtual Boundry under which .NET Program(EXE) is executed

### by default all DLL are InProc 

 - if all EXE in Host process want a certain DLL for execution, we can load it 
   - not at same place at whicg CLR is loaded in host, if it is corrupted,clr can get corrupt
   - yes we can safely load it in host process ,**we will see it later**
   -  so all Inproc process(DLL) get itsspace in application EXE 
   -  all OutProc process(DLL),get memory in CLR host EXE space
  

!['clr-host'](CLR-host-process.jpg)

!['dll-registry'](DLL-registery.jpg)

## command to open VS 2019
```run
devenv 
(to use in Run)
```

### Files in .NET Project

1. Solution
 - consist of group of projects
 - - extension .sln
    - project contain DLL and EXE files
      1. code file, with extension of language 
      - e.g .cs for C#
      2. Properties
      3. References
      4. App.config 
2. compile Solution
- it will compare(compile) all the projects as it is group of project


### Namespace
1. logical grouping of classes
2. e.g 
 ```
 like these many system. classes makes  a DLL ,

 System.Data.DLL 
{
   namespace System.Data
   {
 
        namespace OLDDB
        {
          class OldbConnection{}
        class OldbCommand{}
          class OldbDataReader{}
        }
        
        namespace ODBC
        { 
          class ODB----- {} 
         class ODB -----{}
          class ODB----- {}     
        
        }
        
        namespace SQLClient
        {
          class Sql-----   {}
          class Sql-----   {}
          class Sql-----   {}
        
        }

   }
}
 ```
3. many more DLLs will be made available to you after installation on .NET Framework 
- so .NET Framework = DLLs + CLR
3. this DLLs are called as **Framework Class Libraries (FCL)**
   - DLL always referred in a project ,are called as Base Class Libraries(BCL) ,
   - they are in References Folder, they are
   1. System.Data.Dll
   2. System.Dll
   3. System.Xml.Dll 
  
### demo1
!['demo1'](demo1.jpg)

1. **JIT(<u>J</u>ust <u>I</u>n <u>T</u>ime) Compiler**
- two types
   1. **Standart JIT -- default compiler**
     - EXE have MSIL code,
     - which we give to JIT compiler, to convert it to Binary code,
     - and deliver it to Ram,
     - it gets executed  now  
  
   2. **Pre JIT Compiler**
  
    - how to call pre jit 
     ```
      ngen ___.EXE
    ```
    -  

   3. EKONO JIT --deprecated(not used)  

#### path of developer command prompt, if not installed ,copy from another .net installation
```
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsMSBuildCmd.bat"
```
### to see MSID of your EXE file 
```
 ildasm E:\DAC\.NET_module\Classwork\Day_2_1_Oct\demos\Calci\bin\Debug\Calci.exe

 ildasm : intermediate language disassemble aseembly
 ildasm file path

 ```

 ### Day3 

 1. dll program are meant for reference , it contains method and class,they do not have main function 
 2. while delivering to user, we send .cs (code file) and local DLL file we created.
   
   
 ### Day 4
 
 1. Question - you are  a developer in a company Infosys , you are asked to design classes considering below requirement
  - Client wants to build a simple reporting application
  - Client want to build a software which will generate reports in the form of PDF,Excel,Word,etc.(THis list extension wil grow...)
  - Each report default listed above i.e PDF,Excel,Word needs to be generated using some process

  - the process consists of few logic/funcions to be called in sequence 
  - Details below: 
  -  1. Developer needs to Parse the data given for report generation
  -  2. Developer need to Validate the data given for report generation
  -  3. Developer needs to finally save data in the given extension format i.e PDF<WOrd,Excel
  -  
  -  Design the classes on above requirement 
  - ANS
   1. identify node level classes, as common classes, top to bottom, node to root level classes
   2. main is static, to call static, static is required,so function is static      
   3. **SRP** 
     - ***Single Responsibility Principle*** 
     - it states Entity that you design must have logically single responsibility
     - Entity must have signle reason to change 

### CANT WRITE METHOD DIRECTLY IN NAMESPACE, IT MUST BE CLASS OR INTERFACE

 2. sanket is a software engineer in Amazon.
 - he is given a task of creating application which can handle "Insert/Update/Delete" Functionality
 -  Initial consideration is SQL Server and Oracle 
 -  Final End User needs to be asked for choice of database
    -  after selection of database;
    -   user can pick operation on the database like insert/update/delete
 - sanket needs to ensure the we have code is easy to maintained
 - sanket needs to implement this problem statement using Single Responsibility Principle. 

### example of SRP and best industry practice 

<img src="SRP-exampleOfReport.jpg"> 
<hr> 

# Day5

1. to declare data member ,getter and setter in .net (intellisence)
```
propfull + tab + tab 

```

### Need for Singelton 

!['singleton'](need_for_singleton_project_example.jpg)

<hr> 


1. for  constant fixed footprint of memory
2. for logging use Logger class object as singleton 
for any language, java,c++,in .net with C#.
  

### Value type vs Refrence Type

|Value |Refrence|
|------|--------|
|||


## datatype on heap are called refrence type 
- datatype get memory on heap are Object and String
- compiler creates shorthard for  them, object, string 
!['heapdatatype'](why-dataype-on-heap.jpg)

1. **OBJECT**
- it is a universal type in .NET
- object is a type in .net that can hold any datatype ,user defined as well as default type.
```c#
object o = 100;

```
### casting /unboxing 
- conversion between  heap datatype from/to stack datatype
-  heap to stack conversion is caleed **unboxing**
```c#
  Main()
  {
    string s = "100";

    int i = Convert.ToInt32(s);
  }

```
```c#
   object i = "100";

   int m = Convert.ToInt32(i);
```

- **boxing** is  taking stack datatype  to heap datatype.
```c#
int i = 100;
int s = 200;

object o = 100/11;
```
!['boxing/unboxing'](boxing.jpg)

<hr> 

# Day6

1. Q. Define one array of size 5, of what type ,you decide,
   - the Array will store objects of type Employee and Customer
   - Employeee class will have NO,Name,DeptName
   - Customer will have No,Name,OrderDetails
   - All all Employee and Customer objects are added in the array ;
   - use for loop to print the details of Employee and Customer
   - identify and usage of any other classes like base derived is permitted
  


 2. ArrayList 
    - is a collection of object so it can store anything 
    - to add an value use Add method
    - for length of ArrayList use Count method
  ```c#
  
            ArrayList arr = new ArrayList();
            arr.Add(e1);
            arr.Add(100); // Boxing
            arr.Add("abcd");
            arr.Add(e2);
            arr.Add(false); //Boxing - to heap from stack
            arr.Add(DateTime.Now.ToString());

            arr.Add(new int[] { 10, 20, 30 }); //unkonwn type


            for (int i = 0; i < arr.Count; i++)

            {
                
                if (arr[i] is int)
                {
                    int k = Convert.ToInt32(arr[i]); //unboxing
                    Console.WriteLine(k);
                }
                else if (arr[i] is string)
                {
                    string s = Convert.ToString(arr[i]);
                    Console.WriteLine(s);
                }
                else if (arr[i] is Emp)
                {
                    Emp e = (Emp)arr[i];
                    Console.WriteLine(e.EmpDetails());
                }
                else if (arr[i] is bool)
                {
                    bool b = Convert.ToBoolean(arr[i]); //unboxing 
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type used");
                }

            }

            Console.ReadLine();

  ``` 
  
3. Menu Driven Programme 
- 1.    Ask user whose data you want to enter
- 2. Employee class {- int No,- string Name,+ GetEmpDetails()}
- 3. Book  class   {- string Title, - string Author,GetBookDetails() }
- 4. After this create one Employee object store No and Name i.e 1 and suraj into that object nd put that object in the Arraylist
- 5. then ask user if he wishes to continue ? y/n 

<hr>

!['menudriven'](manu_driven_case_study.jpg)
 
 ----------
 
4. 2D collection(key - value) **Hash Table**
 - key should be of same datatype for all pairs of key-value.



!['hashtable'](hash-table.jpg)
----------


- using foreach for hashtable 
  ```C#

  int [] arr = new int[] {10,20,30,40,50,60};

  // for loop
  for(int i = 0; i < arr.length;i++)
  {
    int data = arr[i]
     console.write(data)
  }
  // using foreach
  foreach(int data in arr)
  {
    console.write(data)
  }

  ```

!['for-vs-foreach'](difference-for-foreach.jpg)


```c#

foreach(object key in arr.keys)
{
  if(someobject is int)
  {

  } else if(someobject is emp)
  {

  }
}
```

!['foreach-for-hashtable'](foreach-hash-table.jpg)

----------

# Day 7

- we have done 
1. int type Array--- --- allows only integers --------------fixed size
2. Emp type Array -------allow only emp---------------------fixed size
-  **(here onwards boxing and typecasting is in bulk)**
1. object type Array --- allow any type of data ----------- fixed size
2. ArrayList --- --------allow any type of data ------------no fixed size,finding some specific thing will need looping 
3.  Hash Table --------- allow any type of data with key----no fixed size,finding some specific thing will neneed key

- generic,generic collectiosn - year 2005 (C# 2.0)
  
 # Todays Agenda 

1. Interface
  - can only consist od declaration
  - abstract class can have both declaration and implementation(method) 
  - or interface use interface key instead of class , add add by usign : to child class, no need to use for method abstract and override keyword
  - method in interface cannot have access specifier like public,private
  - in interface we dont have logic (method) to inherit,still it called  multi-level inheritence is allowed its exactly implementation of method declaration in child class, multiple inheritence is  allowed , but they are compulsory to implement.
  - in interface ,its implementation in child class are compulsory to use, it is not inheritence as it parent class have only declaration  ,so no logic is inherited like (method body).
  
```c#
 public interface A
    {
        void m1();
    }

    public interface B
    {
        void m2();
    }

    public class C : A, B
    {
        public void m1()
        {
            throw new NotImplementedException();
        }

        public void m2()
        {
            throw new NotImplementedException();
        }
    }

```

-  2 or more  interface have same method then we can make method declaration explicit 
-  
  ```c#
  public interface A
    {
       int Add(int x, int y);
        int Sub(int x, int y);
    }

    public interface B
    {
        int Add(int x, int y);
        int Mult(int x, int y);
    }
      public class SomeBase
        { }
        
    public class Maths : SomeBase,A, B
    {
        int A.Add(int x, int y)
        {
            return x + y;
        }

        int B.Add(int x, int y)
        {
            return x + y + 100;
        }

        int B.Mult(int x, int y)
        {
            return x * y;
        }

        int A.Sub(int x, int y)
        {
            return x - y;
        }

        public int div(int x,int y)
        {
            return x / y;
        }
    }

    --------------
        main()
       A obj = new Maths();
           B obj2 = new Maths();
            int result = obj.Add(10, 20);// result = 30
            Console.WriteLine(result);

            int result2 = obj2.Add(10, 20); //result = 130
            Console.WriteLine(result2);

            Console.ReadLine();
  
  ```

- when there is no body in class, only declaration needed use interface , if it has some incomplete implementation  and logic (method) needed use abstract  class




2. Template in C++
- used for creating generic datatype code , as type is a parameter here 
```c#


public class Maths<T>
    {
        public int Add(int x,int y)
        {
            return x + y;
        }
        public void Swap( ref T x, ref T y)
        {
           T z;
            z = x;
            x = y;
            y = z;
        }

       /* public void Swap(ref double x, ref double y)
        {
            double z;
            z = x;
            x = y;
            y = z;
        }*/
    }

    --- main()
    
            Maths<double> obj = new Maths<double>();
          double p = 100.5;
            double q = 200.5;
          obj.Swap(ref p,ref q); // now p = 200.5 q = 100.5

          //same can be done for
          Maths<int> obj2 = new Maths<int>();
```

- in C++ 
  ```c++
  public class Maths<T>
  {
    public void Swap(Tx,Ty)
    {
      // ---logic of swap
    }
  }

  Main()
  {
    Swap(int,int);
    Swap(char,char);

  }

  ```
  - in c++ for each swap object call, one definition of swap in binray will be created


3. Generic in c# is same as Template in C++
- 
```C#
 public class Maths<T>
  {
    public void Swap(Tx,Ty)
    {
      // ---logic of swap
    }
  }

  Main()
  {
    Swap(int,int);
    Swap(char,char);

  }
```
- in C#, when swap is called, first swap definition will go to --> Language Compile ----> MSIL 
- this hapens only once
- now CLR is called it takes-->MSIL-to->JIT compiler--convert it to-> binary definition of swap 
- so as two compiler in .NET Lan and JIT, 
template method binary generation is done at compilation time by JIT , not runtime generation
 - this template in c# is called **Generic** 
- it compile on MSIL only for one template method 

!['template'](GenericvsTemplate.jpg)

- a normal class can consist of normal class and generic method


4. Generic collections
- for this use 
```c#
using System.Collections.Generic;
```
1. List 

```c#
//Math<int> obj = new Maths<int>;

   // unlimited element of a same type given in <>

    List<Emp> arr = new List<Emp>();
    arr.Add(e1);
    arr.Add(e2);
     arr.Add(e3);

   foreach (Emp emp in arr)
     {
      Console.WriteLine(emp.getDetails());
     }
 
```

   - 1. List as ArrayList
      
      ```c#
       //same as Arraylist, use List as ArrayList
         List<object> arr = new List<object>();
      
      ```
   - 2. Use List like ArrayList, List of generic type 
  ```c#
            List<object> arr = new List<object>(); 
            List<Utility<int>> utilities = new List<_00Demo.Utility<int>>();
  ```
   

2. Stack
 ```c#
   //LIFO
   Stack<Emp> arr = new Stack<Emp>();
   arr.Push(e1);
   arr.Push(e2);
   arr.Push(e3);

  Emp e =   arr.Pop();
  Console.WriteLine(e.getDetails());
   Emp v = arr.Pop();

   Console.WriteLine(v.getDetails());

```

3. Queue

```c#
 Queue<Emp> arr = new Queue<Emp>();

 arr.Enqueue(e1);
 arr.Enqueue(e2);
 arr.Enqueue(e3);
 Emp e = arr.Dequeue();

Console.WriteLine(e.getDetails());

 foreach (Emp emp in arr)
   {
    Console.WriteLine(emp.getDetails());
  }
```


4. Dictionary

```c#
 Dictionary<int, Emp> arr = new Dictionary<int, Emp>();
 arr.Add(1, e1);
 //arr.Add(2, e2);

 Emp e = arr[3];

Console.WriteLine(e.getDetails());
         ---------------------------
 Dictionary<int, Emp> arr = new Dictionary<int, Emp>();
  arr.Add(e1.No, e1);
   arr.Add(e2.No, e2);
   arr.Add(e3.No, e3);

   //Emp e = arr[3];
   //Console.WriteLine(e.getDetails());

 foreach (int key in arr.Keys)
   {
    Console.WriteLine("Data available at key " + key.ToString());
     Emp e = arr[key];
    Console.WriteLine(e.getDetails());
   }
```


# File I/O in .NET c#:
- nned System.IO
1. Stream class are for creation , class create data , it is a form of data
2. streamWriter and StreamReader are helper class to manupulate data

types 

1. FileStream
- 1. write a string
```c#
 // FileStream fs = new FileStream("E:\\DAC\\claswork-bofore-git-commit\\Day7\\Data.txt")
 // declare stream with path,mode,access
 FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day7\Data.txt",
                                 FileMode.OpenOrCreate,
                                 FileAccess.Write);
 // need writer to write to file , for stream object fs 

   StreamWriter writer = new StreamWriter(fs);
 //use writer
   writer.WriteLine("hello world ");
  fs.Flush();  // forcefully write on harddrive if data is still in Ram not written due to small data size
   writer.Close();
 fs.Close(); // content in memory //ram still need to be written to file for small data written so use close 


```
- 2. read string

```c#
// declare stream with path,mode,access
            FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day7\Data.txt",
                                           FileMode.Open,
                                           FileAccess.Read);
   // need reader to read from  file , for stream object fs 
   StreamReader reader = new StreamReader(fs);
   //use reader
   string entireData = reader.ReadToEnd();

  Console.WriteLine(entireData);

   reader.Close();
  fs.Close();

Console.ReadLine();
```
3. - writing a object to file  , it doesnt work need another stream helper for serialization and deserialization here
```c#
 it doesnt work --- only writes class name in file rather than class object to file
 #region Object writing to File 

 Emp emp = new Emp();

 Console.WriteLine("Enter No:");
  emp.No = Convert.ToInt32(Console.ReadLine());

  Console.WriteLine("Enter Name");
  FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day7\Data.txt",
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);
StreamWriter writer = new StreamWriter(fs);
  writer.WriteLine(emp);

 fs.Flush();
  writer.Close();
  fs.Close();

   #endregion

```
- Serialization 
- putting your custom class object on Hdd, converting of object into stream of bit and bytes
- DeSerialization
- converting stream of bit of  object of class to object of class from hdd. 

# Day8

  - **todays** 

!['first-whiteboard'](day8-1.png)
<hr>

#### note
  - Assembly
     - **Assembly metadata** - check which section a thing we are looking are located
     - **Resouces** - references , dll, all we have reffered
     - **Type Metadata** - extra type data
     - **MSIL** - type data in MSIL format



  1. Simple text read write
   
  2. **Serialization** (putting objects of some class(es) in the file), 

  3. **DeSerialization** - (getting objects of some class(es) from the file)

  4. types of S/DS

- 1. Binary
    -  used in intranet network , for object storing in files , .Dat,.bin -- extension for binary serialized file
    -   for this need to include
    -   it S private members only , not public like method 
    -   used in TCP heavily ,need to configure for firewall 
    
   ```C#
      using System.IO;
     using System.Runtime.Serialization.Formatters.Binary;
   ```
   - error due to not use Serialize on class, 
    ```
    System.Runtime.Serialization.Serialization Exception
     HResult=0x8013150C
     Message=Type 'demo4.Emp'  is not marked as    serializable.
     Source=mscorlib i.e CLR
    ```
   - need to do this for error to go 
   -   [Serializable] it cant be inherited 

   ```c#
   [Serializable]
    public class Emp
    { 
      // if something need not be included in file use unS
        [NonSerialized]
        private string _Password = "suraj@123";
    }
    
   ``` 
   - Serialization
  ```C#
            Emp emp = new Emp();

             Console.WriteLine("Enter No:");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             emp.Name = Console.ReadLine();


             FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data.txt",
                                             FileMode.OpenOrCreate,
                                             FileAccess.Write);

             BinaryFormatter specialWriter = new BinaryFormatter();

             specialWriter.Serialize(fs, emp);
             //stack pointer , same as  close for writer 
             specialWriter = null;

             fs.Flush();

             fs.Close();
  ```

   - DeSerialization
   
```C#
            FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data.txt",
                                            FileMode.Open,
                                            FileAccess.Read);

            BinaryFormatter specialReader = new BinaryFormatter();

             object obj =  specialReader.Deserialize(fs);
            
            if(obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine( e.getDetails());
            }
            else
            {
                Console.WriteLine("unknown");
            }

            specialReader = null;
            fs.Flush();

            fs.Close();

```
- 2. XML
   -   service based IT company to put data to files
   -   Xml can be anywhere as no virus, simpletext can bypass firewall 
   -   XMl S all public methods only ,no private data member
  - need to remove [Serializable] if in file  
   ```c#
   [Serializable]
    public class Emp
    { 
      // if something need not be included in file use unS
        [NonSerialized]
        private string _Password = "suraj@123";
    }   

  - serilization 
 ```c#
       // XMl Serialization 
             Emp emp = new Emp();

             Console.WriteLine("Enter No:");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             emp.Name = Console.ReadLine();


             FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data.xml",
                                             FileMode.OpenOrCreate,
                                             FileAccess.Write);
             // in xml here, getter and setter, public method i.e public member get serialized  
             XmlSerializer specialwriter = new XmlSerializer(typeof(Emp));

             specialwriter.Serialize(fs, emp);

             specialwriter = null;


             fs.Flush();

             fs.Close();
            

  ```

  - Deserilization 

```C#
  
            FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data.xml",
                                            FileMode.Open,
                                            FileAccess.Read);
            // in xml here, getter and setter, public method i.e public member get serialized  
            XmlSerializer specialReader = new XmlSerializer(typeof(Emp));

            object obj = specialReader.Deserialize(fs);

            if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.getDetails());
            }
            else
            {
                Console.WriteLine("Unknown type of data!!");
            }

            specialReader = null;




            fs.Flush();

            fs.Close();
   

  ```  

- 3. SOAP 
  -   Simple Object Access Protocol
  -   it is same as XML private member S/DS
  - for web application ,services here,SOAP getting used internally
  - it is not practicaly user , unless creating a framework ,
  - as there are sevral helper classes in server side language like java, .net 
  - Serialization
 ```c#

 ```
 - Deserialization
 ```c#

 ```
  !['SOAP-S/DS'](SOAP-S-DS.jpg)


5. case_Study 
!['object s/ds'](casestudy-objectS-DS.jpg)
 - Serialization
 ```c#
Emp e1 = new Emp();
            Book b1 = new Book();

            Console.WriteLine("Enter No:");
            e1.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            e1.Name = Console.ReadLine();


            Console.WriteLine("Enter ISBN :");
            b1.ISBN = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter book title");
            b1.Title = Console.ReadLine();

            ArrayList arr = new ArrayList();

          arr.Add(e1);
           arr.Add(b1);



            FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data2.txt",
                                            FileMode.OpenOrCreate, FileAccess.Write);

            BinaryFormatter writer = new BinaryFormatter();


            writer.Serialize(fs,arr);



            writer = null;

            fs.Flush();
            fs.Close();
 ```
 - Deserialization
 ```c#
FileStream fs = new FileStream(@"E:\DAC\claswork-bofore-git-commit\Day8\Day8\Data2.txt",
                                            FileMode.Open, FileAccess.Read);

             BinaryFormatter reader = new BinaryFormatter();

           

       object obj  =  reader.Deserialize(fs);


             if (obj is ArrayList)
             {
                 ArrayList arr = (ArrayList)obj;
                 foreach (object o in arr)
                 {
                     if (o is Emp)
                     {
                         Emp e = (Emp)o;
                         Console.WriteLine(e.getDetails());
                     }
                     else if (o is Book)
                     {
                         Book b = (Book)o;
                         Console.WriteLine("Details of the book : " + b.Title);
                     }
                 }
             }
             else
             {
                 Console.WriteLine("Unknown type of data!!");
             }



             fs.Flush();
             fs.Close();
 ```

6.    when code is compiled, in Type Metadata Section:     
-  we get a Assembly of class AssemblyInfo object[] for (dll or exe)
  - 1. for classes
   -  which contain a type[] object  for each class/interface in dll or exe (or code compiled) , it stores extra information for each class
   -  AssemblyInfo(dll or exe) has Type[](class)
- - 2. for methods
   - we will get a class  MethodInfo onject    
   - now Type[] (class) has MethodInfo 
!['Type-Metadata1-forClass'](type-metadata-typeForClass-1.jpg)

!['Type-Metadata1-forClass+forMethod+forPParameters'](type-metadata-section.jpg)

- system.reflection class gives intellisense in .NET 
- give signature, call the code 
- locking of source code is called ofescation of sc can be seen by app decompile , not reflection  
- so we cant see source code by reflection 

# Day9

1. Serializable is called Attributes

```forserializable
- E:\DAC\.NET_module\Classwork\Day8_9_Oct\Day8_NET Framework Class Library_Assemblies\MathsLib\bin\Debug\MathsLib.dll
```
## table mapped as classs
!['mapping'](mapping-table-to-class.jpg)
1. first design database , then make a class
1. table name = class name(Model/POCO( plain old CLR object)/POJO/Entity)
2. table column name = getter and setter
3. table row = class object
4. now we can make  List of Class object , and use it at Business layer ---> presentation
   

## Event and Delegates

1. **Delegates** internally are function pointers
2. Event 
  !['POCO'](reflection-by-attribute-class.jpg)



### use to delegates/ function pointer
1. to create a  callback mechanism i.e asynchronous call
  !['usePointerForCallback'](callback-by-pointer.jpg)
2.   for loose coupling 

# Day 10

!['c#features'](CSharpFeatures.jpg)

1. **Partial class (for one file its partial )**
  - one class in multiple files  
  - used for Version Control , between multiple developers
  - 
 !['partial-class'](partialClassDemo.jpg) 

2. **nullable Type** 
  - reference type can hold null value .
  - value type cannot hold null value 
  - e.g
  ```c#
  object obj = null;
  string s = null; 
  -- above are allowed

  int i = null; -- not allowed

  ```  
  - use nullable 
  ```c#
    int?  Salary = 0;
            Salary = null;

           int? Salary1 = 110;
            //both are same 
            Nullable<int> Salary2 = null;

         if(Salary1.HasValue)
            {
                Console.WriteLine( "Salary holds a value" );
            }
         else
            {
                Console.WriteLine("Salary is null");
            }
            Console.ReadLine();
        }

  ```

!['nullable-demo'](NULLABLEdemo.jpg)

3. Anonymous method
```c#
            // calll normal method 
            string message = SayHello("suraj");
            Console.WriteLine(message);

            // SayHello("suraj");
            // call method using  pointer
             mydelegate pointer1 = new mydelegate(SayHello);
             string message2 = pointer1("suraj porje");
              Console.WriteLine(message2);

            //anonymous method
            mydelegate pointer2 = delegate (string name)
            {
                return "Hello" + name;
            };


            string message3 = pointer2("suraj porje");
            Console.WriteLine(message3);

```

4. Lambada Expression 
  - => (goes to symbol)
```c#
 /* 
          mydelegate pointer1 = new mydelegate(SayHello);
          string message2 = pointer1("suraj porje");
          Console.WriteLine(message2);
          */

            //Lambda expression Syntax
            // same as above code in comment
            mydelegate pointer2 = (name) => {
                return "Hello" + name;
            };


            string message3 = pointer2("suraj porje");
            Console.WriteLine(message3);
            

```

5. Iterator concept implementation
- we use foreach fo this
- it implements IEnumerator GetEnumerator() function for this internally class IEnumerable
    {
```c#   

main()
{
  Week week = new Week();

            foreach (string day in week)
            {
                Console.WriteLine(day);
            }
}


 public class Week:IEnumerable
    {
        private string[] days =
            new string[] { "mon", "tue", "wed","thur","fri","sat","sun" };

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[i];


            }


           // throw new NotImplementedException();
        }

    }
```

6. Implicit Type 
  - here we use var keyword , and assign a valu, it implicitely assign datatype to it
  - based on RHS  assignment, LHS datatype is assigned.
  - implicit must be initialized 
  - var is always a local variable 
  - var cannot be only declared, need to be initialize
  -  when you dont know what will be on LHS datatype use var , example if function return a object type 
  -  var differientiate between heap and stack type , which object doesnt and only stores on heap, so better than onject
  -  cannot assign null to implicit type 

```c#

   main( )
   {
          //normal declaration
            int i = 100;
            object obj = i;
            Emp e1 = new Emp();

            obj = e1;
            //implicit declaration
            var v = 100;
           // v = "a"; // gives error

            var v2 = new Emp();

           // var v3; // gives error implicit must be initialized
           

            Console.ReadLine();
        

         //  var v4 = 100; //var is always a local variable 
      ---// use of var 
    int choice = Convert.ToInt32(Console.ReadLine());
            var v5 = GetSomething(choice);

    //var v6 = null; //cannot assign null to implicit type 

   }


  ---
  //function
   public static object GetSomething(int i)
        {
            if(i == 1)
            {
                return 100;
            } else
            {
                return new Emp();
            }
        }
```


7. Auto property
  - use **prop** command for this. 

```c#
main()
{
   Emp emp = new Emp();

            emp.No = 1;
            emp.Name = "suraj";

            Console.WriteLine(emp.Name);

}


// class 

 public class Emp
    {// Auto Property 
        // use prop
        public int No { get; set; }
        public string Name { get; set; }

        public string Address { get; set; }

    }

```

8. Object Initializer 
  - use at class object creation - { setters are called here e.g No = 100,...};

```c#
  //shorthand for assigning values
  //Object Initializer  syntax
   var emp = new Emp() { No = 100, Name = "suraj", Address = "nasik" };
            var emp2 = new Emp()
            {
                No = 100,
                Name = "suraj",
                Address = "nasik"
            };

            Console.WriteLine(emp.Name);

```


9. Autonomous Type
  - commpiler creates a class, which we cannot use other place
  - we can see in MSIL 
   - getter are defined, setter are not,it is not allowed to assign to class object -- i.e it is read only 
   -  so datatype of object of anonymous class must be var, as we have no knowledge about type of data member
   -  so var at compile time get datatype of object returned, so we need to use var, as we dont know what it on RHS
   -   both class object have one generic class ,with generic datamember, as parameter sequence matter, values doesnt
```c#
      // commpiler creates a class, which we cannot use other place
      // we can see in MSIL 
       // getter are defined, setter are not,it is not allowed -- i.e it is read only 
      // so datatype of object of anonymous class must be var, as we have no knowledge about type of data member
      // so var at compile time get datatype of object returned, so we need to use var, as we dont know what it on RHS

      var emp = new { No = 100, Name = "suraj", Address = "nasik" };
           
     // object emp = new { No = 100, Name = "suraj", Address = "nasik" };

            var emp1 = new { No = 100, Name = "suraj", Address = 1 };

     // both class object have one generic class ,with generic data member
     //now it creates a new class in MSIL for this 
    // as parameter sequence matter, values doesnt

      var emp2 = new { No = 100, Address = 1, Name = "suraj" };
      Console.WriteLine(emp.No);
      Console.WriteLine(emp.Name);
      Console.WriteLine(emp.Address);

```

# Day 11

- **C# features continued...**
 
10. sealed class and method
-  sealed keyword is used on leaf classes or end classes, to avoid creating a exe by inheriting from our product classes, to make duplicate product exe .
-  it is used by product based companies,providing license for product, for activation .
-  sealed class 
  - cannot be inherited
  - cannot be changed due no source code    

```c#
//String is sealed class, as part of product package ,so cant inherit from class
        public class MyString:String //error
        {

        }

         #region Sealed  used to forbid inheritence of class

    #endregion

   public sealed class SomeClass
    {

    }

    public class SomeOtherClass:SomeClass //error
    {

    }

    #region Sealed used to stop override of methods
    public sealed class A
     {
         public virtual void M1()
         {

         }
     }

     public class B:A
     {
         public sealed void M1()
         {

         }
     }
     public class C : B
     {
         public void M1() // error 
         {

         }
     }
    #endregion

```

!['sealed'](need-for-sealed-classes.jpg)

11.  Extension method
-   extends to datatype methods 
-   need class and method be static 
-   use this key word while declaring parameter 
-   contains is a method of string to check if text given is there , return bool
-   extends on parameter with this 
-   microsoft provides based on extension method, linq 
```c#
     using System.Linq;
```

# every array comes from IEnumerable 
```c#

main()
{
    Console.WriteLine("Enter some data");
            string data = Console.ReadLine();
            //  bool result = MyUtilityClass.CheckForValidEmailAddress(data,100);

            //  public static bool CheckForValidEmailAddress(this string s,int i)
            // this makes the method a extension of string 

            bool result = data.CheckForValidEmailAddress(100);  // extension method
            Console.WriteLine(result);
            //Sealed 

}



 public static class MyUtilityClass
    {
        public static bool CheckForValidEmailAddress(this string s,int i)
        {



            return s.Contains("@");
          
        }
    }

main()
{
   int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };

            Console.WriteLine(arr.Average());

            Console.WriteLine("Enter some data");
            string data = Console.ReadLine();


            var v = new { No = 1, Name = "suraj", Address = "nasik" };

            //v.CheckForValidEmailAddress(100);


            Console.WriteLine(v.CheckForValidEmailAddress(100));
}

---- generic  extensio method 
 public static class MyUtilityClass
    {
        public static bool CheckForValidEmailAddress<T>(this T s, int i)
        {



            //return s.Contains("@");
            return true;
        }
    }


```

12. Lambada Expression 
# can you write method in anonymous class, ans: not directly, but we can using generic anonymous method
-  using stopwatch method on a result to calculate time taken for method to run - 
   - 1. using normal Call to check - 407 units * 1000(compile time )
   - 2. using Delegate - 289 units * 1000(compile time )
   - 3.  using generic delegate -  382units * 1000(compile time )
   - 4. using genric delegate with anonymous method - 270 units * 1000(compile time )
   - 5.  using Func (inbulit) delegate with anonymous method - 159 units 
      - Inbuilt Delegate
        - **Action delegate**
        - Action ptr = new Action();
        - use Action when return type is void 
        - Action ptr = new Action();
        -  used to point to a method wich return void , and take 16 parameter

        - **Func delegate** 
        - used to point to a method wich can return something all the time , and take 16 parameter  
        
   - 6.  using Func (inbulit) delegate with lambada express  - 170 units (create + compile) 163 unit  + (Execute) 7 unit
   - 7. Compiled Expression tree - 7 units 
```c#
main()
{
  
1. bool result = Check(100);

2.  MyDelegate pointer = new MyDelegate(Check);


3. MyDelegate<bool,int> pointer = new MyDelegate<bool,int>(Check);

4.  public delegate Q MyDelegate<Q,T>(T i);
 MyDelegate<bool, int> pointer = delegate (int i) { return (i > 10); };

5.  Func<int, bool> pointer = delegate (int i) { return (i > 10); };

6.  Func<int, bool> pointer = ( i) => { return (i > 10); }; //lamaada expression

// rest of code 
           Stopwatch watch = new Stopwatch();
            watch.Start();

            //bool result = Check(100);
            bool result = pointer(100);

            watch.Stop();

            Console.WriteLine("Time Taken = " + watch.ElapsedTicks.ToString());
            Console.WriteLine("Result is " + result);

            Console.ReadLine();
}
7. using Expression tree 
main2()
{
              // expression tree
            //   Func<int, bool> pointer = ( i) => { return (i > 10); };
            //stage 1: Create a  Expression tree
            Expression<Func<int,bool>> tree = (i) =>  (i > 10);

            //stage 2: Compile Expression tree
            //compile method keep it in ram 
            Func<int, bool> pointer = tree.Compile();

            // compileToMethod stores method as optimized binary tree in hard drive
           // Func<int, bool> pointer = tree.CompileToMethod();

            Stopwatch watch = new Stopwatch();
            watch.Start();

            //stage 3: in case of expression tree i.e Execute the tree
            bool result = pointer(100);

            watch.Stop();

            Console.WriteLine("Time Taken = " + watch.ElapsedTicks.ToString());
            Console.WriteLine("Result is " + result);

            Console.ReadLine();
}




  //  method
 public static bool Check(int i)
        {
            return (i > 10);
        }

```


!['lambdaExpression-withFuncDelegate'](Expression-tree-flow.jpg)

!['compile+executeTime'](performance-time-for-all-functionCalls.jpg)

13. Language-Integrated Query (LINQ)
-   is the name for a set of technologies based on the integration of query capabilities directly into the C# language. 
-   Traditionally, queries against data are expressed as simple strings without type checking at compile time or IntelliSense support.
-    With LINQ, a query is a first-class language construct, just like classes, methods, events.

- For a developer who writes queries, the most visible "language-integrated" part of LINQ is the query expression. 
- Query expressions are written in a declarative query syntax. By using query syntax, you can perform filtering, ordering, and grouping operations on data sources with a minimum of code.
-  You use the same basic query expression patterns to query and transform data in SQL databases, ADO .NET Datasets, XML documents and streams, and .NET collections.
- syntax
```c#
  from <variable> in <collection>

<where, joining, grouping, operators etc.> <lambda expression>

<select or groupBy operator> <format the results>
```

  ```c#
        class Program
       {
        static void Main(string[] args)
        {
            List<Emp> emps = new List<Emp>()
            {
                new Emp{ No = 11, Name = "Mahesh"  , Address="pune" },
                new Emp{ No = 12, Name = "Rahul"  , Address="panji" },
                new Emp{ No = 13, Name = "Rajiv"  , Address="mumbai" }
            };

            Console.WriteLine("Enter City Search Character");
            string cityFilter = Console.ReadLine();

            // var result = new List<Emp>();

            #region using foreach
             foreach (Emp emp in emps)
            {
                if (emp.Address.EndsWith(cityFilter))
                {
                    result.Add(emp);
                }
            }
            #endregion

            #region using linq lazy loading

           var result = from emp in emps
                          where emp.Address.StartsWith(cityFilter)
                          select emp
            // this is also included in result as
            // query get executed when result is used ,caleed lazy execution of linq
            #endregion

            #region using linq with ToList extension 
              var result = (from emp in emps
                              where emp.Address.StartsWith(cityFilter)
                              select emp).ToList();
                // ToList is extension method, it takes object which we call as first parameter
                //as object i.e first parameter ,need to xecute query here, so ashu not included
                emps.Add(new Emp() { No = 11, Name = "ashu", Address = "manmad" });


                Console.WriteLine("Filtered Result is ---");
                foreach (Emp e in result)
                {
                    Console.WriteLine(e.Name + " is " + "from" + " " + e.Address);
                }
            #endregion

            #region Linq using anonymous class for selecting odd columns 
           var result = (from emp in emps
                          where emp.Address.StartsWith(cityFilter)
                          select new
                          {
                              EName = "Mr/Mrs " + emp.Name,
                              ECity = emp.Address

                          }).ToList();
            //behind the scene of result = 
            // var result = emps.where().select();
            //series of extension method 
            // extension are compiled expression tree, for best performance 

            Console.WriteLine("Filtered Result is ---");
            foreach (var r in result)
            {
                // Console.WriteLine( e.Name + " is " +  "from" + " " +  e.Address) ;
                Console.WriteLine("Name = " + r.EName + " City  = " + r.ECity);
            }
            #endregion
        }
    }

    /*public class ResultHolder
    {
        public string EName { get; set; }
        public string ECity { get; set; }
    }*/



    public class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }


```

# Day 12 
14. 00 Pratial methods
- in .NET, we can write methods with declaration/use in one file ,and its defination /implementation in another file, considering both methods are part of same partial class, and method itself are partial , this is partial method
- if partial method are only declared and no definition is written it ,it is skipped while execution 
```C#
namespace demo1
{//developer 1
   public partial class Emp
    {
        private int _Age;

       partial void  Validate(string propertyName, object Value);
        public int Age
        {
            get { return _Age; }


            set 
            {
                Validate("Age", value);
                _Age = value;
            }
        }

    }
}

namespace demo1
{//developer 2 
    public partial class Emp
    {
        partial void Validate(string propertyName, object Value)
        {
            if(propertyName == "Age")
            {
                int age = Convert.ToInt32(Value);
                if(age < 18 || age > 60 )
                {
                    Console.WriteLine("Age is set up wrong !");
                }
            }

        }
    }
}



```

### Two approaches to connect to database from program use(ORM (object relational Mapping)) 
1. **poco first** 
- i.e first class is created and database values are attributed to th class, and then db table is created
2. **Database first**
-  i.e here the poco/Entity/Model Layer is created after the db table generation, also poco can be automatically generated using
  tools/framework --> called ORM 
  >  Hibernate /  N Hibernate(Red hat) /   Entity Framework (Microsoft)
  - it created partial class for database (table)
  - as developr we need to complete the methods using a partial class in ORM
- **use of partial class**
   - some partially automatic generated code and some partially developer written code ,here  partial method are helpful 
!['ORM-use'](partial-class-ORM.jpg)

15.    **Dynamic** (datatype can be used instead of var or obj)
-  var get datatype at compile time, 
- dynamic type get datatype at runtime, so no intellisense ,as datatype not known 
-   uses instead of object, to get 
-  dynamic get definition/datatype at runtime , var get at compile time 
-  dynamic uses reflection to get object type ,so we dont need to type cast object  to class object, but reduces performance 
-   **used in** MVC,WPF,entity framework .  

```c#
 --- class
    public class Factory
    {
        public  object GetMeSomeTypeObject(int choice)
        {
            if(choice == 1)
            {
                return new Emp();
            }
            else
            {
                return new Book();
            }
        }
    }

main()
{


// using dynamic type 
Factory factory = new Factory();
            Console.WriteLine(" Enter choice as number ");
            int choice = Convert.ToInt32(Console.ReadLine());
            //dynamic uses reflection 
            dynamic obj = factory.GetMeSomeTypeObject(choice);

            Console.WriteLine(obj.GetDetails());

// using object factory on object 
  Factory factory = new Factory();
            Console.WriteLine(" Enter choice as number ");
            int choice = Convert.ToInt32(Console.ReadLine());
   
            object obj = factory.GetMeSomeTypeObject(choice);

   if(obj is Emp)
             {
                 Emp e = (Emp)obj;
                 Console.WriteLine(e.GetDetails());
             }
             else if(obj is Book)
             {
                 Book b = (Book)obj;
                 Console.WriteLine(b.GetDetails());
             }       

}

  

```

16.  **optional and named parameters**
- using string.format,
- using placeholder in method definition makes it a optional/named parameter
- using string.format we dont need to use + for variables , can use {x} as a placeholder for variable 
- no need to overload parameters, for multiple values ,we can use named parameter or optional parameter
 ```c#

   main()
   {
      #region optional and named parameters
           Player player = new Player();

            string detail = player.GetDetails(1, "suraj", "nasik");

            Console.WriteLine(detail);
            string detail2 = player.GetDetails(1, name: "suraj");

            Console.WriteLine(detail2);
            string detail3 = player.GetDetails(1, address: "kolkata");

            Console.WriteLine(detail3);
            #endregion
   }


---class
 public class Player
    {
        public string GetDetails(int no, string name="ABCD", string address="mumbai" )
        {
            return string.Format("Details are : No = {0}.  name = {1} and Address is {2}", no, name, address);

        }

        /* public string GetDetails(int no,string name,string address)
         {
            return string.Format("Details are : No is {0}. Your name is {1} and Address is {2}",no,name,address);

         }*/
    }

```



###  Microsoft Sql Server

1.  command see bg services in Run 
  - to know all services running on windows 
  - so when you create connection pool,  you tell os to connect to service mysql always running in background
    - where localhost tells, mymachione/portno you need to use   
```
// use in run to get all services on windows
services.msc
```

2. type of Sql Server
  - 1. Standard Edition  - Paid, one machine license

  - 2. Enterprise Edition - pais, multiple installation (s).suitable for companies
  - 3. Express Edition - free edition 
     - Download and Install on Machine
     - Install during Visual Studio Installation 
     -  DB size 
         - on free edition , Max Db size can be between 2 GB - 10 GB based version    
3. Installation of Standard /Enterprise Edition

!['microsoft-sql-server-installation'](sql-server-installation.jpg)


 - default instance name of sql server  
    > (LocalDB)\MSSQLLocalDB
 - connection string for sql server     
  - > "Server = (LocalDB)\MSSQLLocalDB; database=SunbeamDB;Integrated Security = true "
 - opt 2
  - > "Data Source = (LocalDB)\MSSQLLocalDB;Initial Catelog = SunbeamDB;Integrated Security = true"
 - Incase of Sql Server Authentication 
 - Instead of <u>Integrity Security</u>
  -  > id = username ; password = password   

!['connection string-or-connection-pool'](connectionstring-or-pool.jpg)

 - can install this to run/manage sql query ,its optional
   - > SQL Server Management Studio (SSMS) i
   - 
- we will  use visual studio for db management, as following    

1. Sql Server Commands use --- oledb,odbc,sqlclient
  - this sql related command  belongs to  System.Data.dll ,to use them , command is
  > using System.Data;
  - commands for db management : showing  used in NodeJs and their .NEt Equivalent 
   1. mysqli_connect  --used in  NodeJs 
     -  connection  
     -  oledbconnection /odbcconnection  
     - **for SQL server ---> can use sqlconnection**
   2. mysqli_query  -  command 
      -  oledbcommand/ odbccommand      
      - fo**r SQL server ---> can use sqlcommand**  
   3. mysqli_fetch_rows - datareader (to read query result )
      - oledbdatareader/obcdatareader    
      -**for SQL server ---> can use sqldatareader**


!['connectToSqlServeruse'](Sql-server-%20createInstance-and-commands.jpg)

5. **SQL Server CRUD Queries**

    1. Select query
     ```c#
      #region Select query
          SqlConnection con = new SqlConnection(@"Server = (LocalDB)\MSSQLLocalDB; database=SunbeamDB;Integrated Security = true ");

            con.Open();

            SqlCommand cmd = new SqlCommand("select * from Emp", con);

               SqlDataReader  reader =   cmd.ExecuteReader();

           while(reader.Read() )
            {
                *//*  Console.WriteLine(reader[0]);
                       Console.WriteLine(reader[1]);
                  Console.WriteLine(reader[2]);*//*

               string data = string.Format("{0}----{1}---{2}",reader["Id"],reader["Name"],reader["Address"]);
                Console.WriteLine(data);
            }

            con.Close();
            #endregion


     ```


    2. Insert Query
     ```c#
      #region Insert query


            SqlConnection con = null;
            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;
                Initial Catalog=SunbeamDB; Integrated Security=True;Pooling=False  ");

                con.Open();

                Console.WriteLine("Enter no");
                int no = Convert.ToInt32(Console.ReadLine());


                Console.WriteLine("Enter name ");
                string name = Console.ReadLine();

                Console.WriteLine("Enter address ");
                string address = Console.ReadLine();

                string queryTemplate = "insert into Emp values({0},'{1}','{2}')";

                string finalInsertQuery = string.Format(queryTemplate, no, name, address);

                SqlCommand cmd = new SqlCommand(finalInsertQuery, con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update .delete

                Console.WriteLine("no of rows affected = " + noOfRowsAffected);


                
            }
            catch (Exception ex)
            {
                Console.WriteLine("something is not right here,try again!!");
                Console.WriteLine("technical detail = " + ex.Message);
               
            }
            finally
            {
                con.Close();
            }

            #endregion
           

     ```
    3. Update
     ```c#
       SqlConnection con = null;

            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                    Integrated Security=True;Pooling=False");

                con.Open();

                Console.WriteLine("Enter employee no");
                int no = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ente remployee Name");
                string Name = Console.ReadLine();
                Console.WriteLine("Ente remployee Address");
                string Address = Console.ReadLine();

                string updateQuery = "update Emp SET Name = '{1}',Address = '{2}' where Id = {0}";
                

                string finalQuery = string.Format(updateQuery, no, Name, Address);

                SqlCommand cmd = new SqlCommand(finalQuery, con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update /delete


                Console.WriteLine(string.Format("no of rows affected = {0}", noOfRowsAffected.ToString()));

            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }
            finally
            {
                con.Close();
            }

     ```
    4. Delete 
     ```c#
     SqlConnection con = null;

            try
            {
                con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;
                                    Integrated Security=True;Pooling=False");

                con.Open();

                Console.WriteLine("Enter employee no");
                int no = Convert.ToInt32(Console.ReadLine());

               

                string deleteQuery = "delete from Emp WHERE Id = {0}";


                string finalQuery = string.Format(deleteQuery, no);

                SqlCommand cmd = new SqlCommand(finalQuery, con);

                int noOfRowsAffected = cmd.ExecuteNonQuery(); //for insert /update /delete


                Console.WriteLine(string.Format("no of rows affected = {0}", noOfRowsAffected.ToString()));

            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }
            finally
            {
                con.Close();
            }

     ```
# Day13

### need for Db - stored procedure
1. **Sql injection** attack 
- using 
```c#
//console user input
1. suraj'OR Name like '%
2. any'OR Name like '%

main()
{
  string constr = @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;Integrated Security=True;Pooling=False";

            SqlConnection con = new SqlConnection(constr);

            Console.WriteLine("please enter name");
            string cmdquery = "select count(*) from Emp where Name = '" + Console.ReadLine() + "'";

            SqlCommand cmd = new SqlCommand(cmdquery, con);

           
            con.Open();

            object result = cmd.ExecuteScalar();

            int count = Convert.ToInt32(result);

            if(count > 0)
            {
                Console.WriteLine("you are a valid user");
            }
            else
            {
                Console.WriteLine("you are not a valid user");
            }

            con.Close();
---      
main()
{
 string constr = @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;Integrated Security=True;Pooling=False";

            SqlConnection con = new SqlConnection(constr);

            Console.WriteLine("please enter name");
          string cmdquery = "select * from Emp where Name = '" + Console.ReadLine() + "'";

            SqlCommand cmd = new SqlCommand(cmdquery, con);

           
            con.Open();
                 
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Console.WriteLine(string.Format("{0} | {1}", reader["Name"], reader["Address"]));
            }
            con.Close();

            Console.ReadLine();
}
            
```

!['for-sql-injection'](for-Sql-injection.jpg)

2. App Configuration
  ```c#
   string sqlconnect = ConfigurationManager.ConnectionStrings["sqlstring"].ToString();

             SqlConnection con = new SqlConnection(sqlconnect);

             --------App.config file------------

             <connectionStrings>
	            	<add name="sqlstring" connectionString="Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=DemoDB;Integrated Security=True;Pooling=False"/>
	             </connectionStrings>


  ```

3. demo for interface of CRUD operation on Emp table
   ```c#

     --------App.config file------------
     <appSettings>
		<!--here 1 means SqlServer-->
		<!--here 2 means OracleServer-->
		<add key="dbchoice" value="1"/>
	
  	</appSettings>
  	<connectionStrings>
		<add name="sqlconstring" connectionString="Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=DemoDB;Integrated Security=True;Pooling=False"/>
  	</connectionStrings>
      -------------------------------------------

      ------------sql- db-operation-----------
         public List<Emp> Select()
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
                List<Emp> emplist = new List<Emp>();
                


                string query = "select * from Emp";

                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    Emp emp = new Emp()
                    {
                        No = Convert.ToInt32(reader["No"]),
                        Name = reader["Name"].ToString(),
                        Address = reader["Address"].ToString()
                    };

                    emplist.Add(emp);
                }

                return emplist;

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return null;
            }
            finally
            {
                con.Close();
            }

        }

        public int Insert(Emp emp)
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
               

                string query = "insert into Emp values({0},'{1}','{2}')";
                string finalQuery = string.Format(query, emp.No, emp.Name, emp.Address);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }

            
        }

        public int Update(Emp emp)
        {

            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {

                string query = "update Emp Set Name = '{0}',Address = '{1}' where  No = {2}";

                string finalQuery = string.Format(query, emp.Name, emp.Address, emp.No);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        public int Delete(Emp emp)
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {

                string query = "delete from Emp where  No = {0}";

                string finalQuery = string.Format(query, emp.No);
                con.Open();
                SqlCommand cmd = new SqlCommand(finalQuery, con);


                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }
  
   ```

4. Data Table and data set
```c#

//for use include 
using System.Data;

  ------------- Data table concept-----------------------------
             DataTable dt = new DataTable("myTable");

             DataColumn c1 = new DataColumn("No", typeof(int));

             DataColumn c2 = new DataColumn("Name", typeof(string));
             DataColumn c3 = new DataColumn("Address", typeof(string));

             dt.Columns.Add(c1);
             dt.Columns.Add(c2);
             dt.Columns.Add(c3);

             dt.PrimaryKey = new DataColumn[] { c1 };

             DataRow r1 = dt.NewRow();
             r1["No"] = 21;
             r1["Name"] = "Chris";
             r1["Address"] = "Nagpur";

             DataRow r2 = dt.NewRow();
             r2["No"] = 22;
             r2["Name"] = "rice";
             r2["Address"] = "Kanpur";

             DataRow r3 = dt.NewRow();
             r3["No"] = 23;
             r3["Name"] = "cansas";
             r3["Address"] = "igatpuri";

             dt.Rows.Add(r1);
             dt.Rows.Add(r2);
             dt.Rows.Add(r3);


     -----------------------------------------------------------------

        ------------- using Dataset concept--------------------------------------- 
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("myTable");

            DataColumn c1 = new DataColumn("No", typeof(int));

            DataColumn c2 = new DataColumn("Name", typeof(string));
            DataColumn c3 = new DataColumn("Address", typeof(string));

            dt.Columns.Add(c1);
            dt.Columns.Add(c2);
            dt.Columns.Add(c3);

            dt.PrimaryKey = new DataColumn[] { c1 };
            //--------------------------------

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=DemoDB;Integrated Security=True;Pooling=False");

            SqlCommand cmd = new SqlCommand("select * from Emp", con);

            con.Open();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                DataRow row = dt.NewRow();
                row["No"] = Convert.ToInt32(reader["No"]);

                row["Name"] = reader["Name"];

                row["address"] = reader["Address"];

                dt.Rows.Add(row);

            }

            con.Close();

            //--------------------------------
            ds.Tables.Add(dt);

            //---------------------------------

            DataRow newRow = ds.Tables[0].NewRow();

            newRow["No"] = 40;

            newRow["Name"] = "milkha";
            newRow["Address"] = "Lahore";

            ds.Tables[0].Rows.Add(newRow);

            //------------Book Table---------------------

            DataTable book = new DataTable("Book");

            DataColumn bc1 = new DataColumn("ISBN", typeof(int));

            DataColumn bc2 = new DataColumn("Title", typeof(string));

            book.Columns.Add(bc1);
            book.Columns.Add(bc2);

            DataRow brow = book.NewRow();

            brow["ISBN"] = 100;
            brow["Title"] = "Let us C#";

            book.Rows.Add(brow);

            //---------------------------------------
            ds.Tables.Add(book);
            //----------------------------------------
           ------------------------------------------------------------------------

```

4.  disconnected architecture
- disconnected way of doing  operation in database using dataset containing datatable 
- use disconnected mode, collecting data from variety of sources and analysis , reporting .
- all above from datatable can be done by sqlAdapter
  
```c#
--------------- Fetch Data using Disconnected Architecture------------------------------
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;
            Initial Catalog=DemoDB;Integrated Security=True;Pooling=False");

            SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from Emp", con);

            dataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

            dataAdapter.Fill(ds, "abc");
      -------------------------------------------------------------------


 ---------------Insert using Disconnected Architecture-------------------
            DataRow newRow = ds.Tables["abc"].NewRow();

             newRow["No"] = 30;
             newRow["Name"] = "Harish";
             newRow["Address"] = "goa";

             ds.Tables[0].Rows.Add(newRow);

             dataAdapter.Update(ds, "abc");
  ----------------------------------------------------------------------

------------------ Update using Disconnected Architcture-------------------
            Console.WriteLine("Enter a Emp No for update");
            int no = Convert.ToInt32(Console.ReadLine());

            DataRow rowToModify = ds.Tables[0].Rows.Find(no);

            if (rowToModify != null)
            {
                Console.WriteLine("Record Found");

                Console.WriteLine("Current Name = " + rowToModify["Name"].ToString());
                Console.WriteLine("Current Address = " + rowToModify["Address"].ToString());
                Console.WriteLine("------------------------------------------");

                Console.WriteLine("Please Enter New Name");
                rowToModify["Name"] = Console.ReadLine();

                Console.WriteLine("Please Enter New Address");
                rowToModify["Address"] = Console.ReadLine();

            }
            else
            {
                Console.WriteLine("Sorry! No record Found");
            }

            dataAdapter.Update(ds, "abc");
      ----------------------------------------------------------------------

------------------ Delete using Disconnected Architecture--------------------
           Console.WriteLine("Please enter Emp No to be deleted");
            int no2 = Convert.ToInt32(Console.ReadLine());

            DataRow rowToDelete = ds.Tables[0].Rows.Find(no2);

            if (rowToDelete != null)
            {
                rowToDelete.Delete();
            }
            else
            {
                Console.WriteLine("Sorry! No record Found");
            }

            dataAdapter.Update(ds, "abc");
   ----------------------------------------------------------------------
```

# Day14
- learned
- Calling a stored procedure
- using Data table and Data set
- Disconnected Architecture
  - SQL COnnection
  - SQLDataAdapter
  - SQLCommandBuilder
  - DataSet
  

### ORM

1. Entity framework
  - used in industry,as standard, rather than disconnected Archi
  - using new item --> data ---> entity model gen ----> db selection ---> gives t4 template
  - reference for .NET and language
     >  https://channel9.msdn.com/


2. ORM  framework
  - Object Relational Mapping Framework
  - hibernate in java 


!['entity-framework'](EntityFramework.jpg)

### Active Server Pages(ASP.NET)
3. where it works
    1. Client/Browser side
      - HTML
      - Javascript
      - Angular

    2. Server Side
      - NodeJS
      - PHP - x.php
      - ASP.NET MVC - aspx
      - JSP/Spring - jsp


## design patteren - common solution for common problem
1.  - 1. like singleton for logging, bradcase
    - 2. Factory - for object creation 
- pattern can be icluded into categories based on web development  
 1. Page controller pattern - must exist
-  page Extension based calls
-   example  of extension 
     - .JSP
     - .ASP --imp .net
     - .PHP
     - .ASPX -- imp

!['page-front-pattern'](page-front-pattern.jpg)
 1. Front Controlled Pattern  
-  call to mapped URI, rather than extension files  
-  examples are  
    - Strusts in java
    - Spring
    - Spring Boot
    - ASP.NET MVC /ASP.NET MVC Core -- img .net
    - CodeIgniter/ Laravel
    - Node JS

### client -service requirement 

!['requirement-for-web-developemnt'](requirement-for-asp-webDevelopment.jpg)

1.Internet Information Services (IIS) (IIS EXpress) 
- just like apache, for production purpose
 -  Vscode Live for development purpose
2. .NET Framework
3. Visual Studio 2019   


### flow of request response of web 
- with solving shared server problem 
  - for php and ASp server   
!['forAsp'](for-php-soln.jpg)


1. Loopback IP - 127.0.0.1 /localhost
 
         

### MVC Life Cycle
!['ASP.NET-MVC-project'](MVC.jpg)


# Day15

### MVC Lifecycle  Customization  by customize object/controller factory  

!['MVC-Lifecycle customization '](customizationOfMVCLifecycle-usingOOP-ObjectFactory-.jpg)
1. go t default web Handler

2. in Route config can set default  
  - controller
   - action  

3. from .html page control goes to  Controller
   -  flow from there on 
  !['controller-flow'](Controller-flow.jpg)
---


# Day16

1. MVC project
  - attribute to know what the method does/route does
 ```c#
  [HttpGet/Post]
 public returntype fuction()
 {}
 ```
   !['dotnetProject'](dotnetProject.png)

 -  Attribute  
```c#
// can be used to send common data to cshtml page
// i.e multiple methods and cshtml having same ViewBag key, so can be used in layout page 
ViewBag["key"] = "data ...";  

ViewData["key"] = "data ...";  

```

 -  Default  Binder
  - can use class as a parameter ,rather than FormsCollection



 -  structure of web pages
  !['web pages'](webpage-structure.png)


# Day 17


> Agenda to complete
1. Attribute based prog for dependency injection 
2. Interface based prog(Action and result filter)
3. Filter 


!['f'](filters-to-learn.png)
- are like middleware, they are classes which we can call with every route 
- they are done with attribute programming 
- types of filter
- 1. Authentication filter
- 2. 
- mainly used for logging 
- implement filter using  Attribute based prog


###  Types  of Filter

  1. **Authentication filter** 
    - implemented before every request
    - can be implemented as default or custom , by specifing in WebConfig file 
    - use attribute to apply auth to a controller class
    ```c#
      [Authorize]
      public class ---- 
      {} 

    ```
 
  -   Types of Authentication 
      1.  **form based  Authentication/ based on server side Session Id**(in angular it was in session storage ,so CLient side A)
         - here  AuthenticationMode = forms,so called form based  Authentication
         - server creates  an object in server side called Session object
              - contains Session Id = Value based on IP/Time/Uname ...
         - this session ID is added to 
          - SETCOOKIE(SessionID) on client side(browser)
              - based on this browser cna store this in memory, as session id related to website 
              - browser can store it ,based on which it has two types -
                   1. Transient cookie - store in memory
                   2. Persistent cookie - stored in HDD    
         - now we send an request  we can verify /auth based on session id on server side and send by browser with request
        
        !['Auth-on-web-by-form'](WEB-Auth-mode-form.jpg)  

       1.    **form auth filer creation method**     
         1. in WebConfig file, nneded to do 
           -  Authorization tag- can be used to allow or deny access
         ```c#
         <authentication mode="Forms">

		     <forms loginUrl="/Login/SignIn"></forms>

	        </authentication>
         ```
         
         2. **Now Implement Auth Class**
            - **require namespaces**
        ```c#
            using System.Web;
            using System.Web.Mvc;
            using System.Web.Security;
        ```

         3. **Sign in method**
              ```c#
            public class LoginController : Controller
            {DemoDBEntities dbObj = new DemoDBEntities(); } 

               // GET: Login
               public ActionResult SignIn()
               {
                  return View();
               }
               [HttpPost]
               public ActionResult SignIn( HRLoginInfo loginObject,string ReturnUrl)
            {

              var MatchCount = (from hr in dbObj.HRLoginInfoes.ToList()
                               where hr.UserName.ToLower() == loginObject.UserName.ToLower()
                               && hr.Password == loginObject.Password
                               select hr).ToList().Count();

              if (MatchCount == 1)
              {
                // since user details are correct
                // create a session ... send cookie to cient 
                FormsAuthentication.SetAuthCookie(loginObject.UserName, false);
                //give instruction to shoot a call for '/Home/Index '

                if(ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else {  return Redirect("/Home/Index"); }
                return View();
                 }
               else{ ViewBag.ErrorMessage = "User Name or Password is incorrect";
                return View(); }  
              }
             ```

          4. **Signout method**
         ```c#  
          public ActionResult SignOut()
          {
            FormsAuthentication.SignOut();

            return Redirect("/Home/About");
          }
        ```  
      

      2. **Passport** based authentication (Session sharing)
       - there is a concept of session sharing of one account on multiple website
          - eg. a google account on  multiple website login - like git, youtube..
         - issue with this was  one website log out made you logout of all website , where session was on 
         - this was kn own as  **passport auth technique**
         - as one accout can be used for multiple website, and single logout ending session   
- 
      3. **Federated** based Auth 
      - based on service based Architecture  
      - for web services
      - 
      4. **Windows** based Auth 
      - check webconfig, then machine config file, for intranet auth
      - uses window auth technique\
      - all user are windows configured user (i.e OS accounts)
      - use for intranet level authentication
 !['auth-windows'](Windows-auth-use-in-intranet.jpg)
 
                   
     
     

  2. **Action Filter**
    - used to execute a certain method before and after a method execution, like logging  
    - two types of Action filter 
        1. ExecutingActionFIlter- can be used to perform a method like logging before a method is called
        2.  ExecutedActionFIlter- can be used to perform a method like logging after a method is called 
  . **Result Filter**
   - used to execute a certain method before and after  UI(page) processing  
    - two types of Result filter 
        1. ExecutingResultFIlter- can be used to perform a method like logging before UI processing
        2.  ExecutedResultFIlter- can be used to perform a method like logging after UI processing
    - implementation of action and result filters
  ```c#
    //compulsory implementation of filters
     action and result filter interface added to class
    public class CustomFilter : IActionFilter, IResultFilter {}

     // or can use a abstract class with logger
      public class CustomFilter : ActionFilterAttribute { }

  ```  
    - **adding filter to base  class**
  ```c#
  [CustomFilter]
    [Authorize]
    public class BaseController : Controller
    {
       protected DemoDBEntities dbObj { get; set; }

        public BaseController()
        {
            this.dbObj = new DemoDBEntities();
        }

    }

  ```

  > **Logger class with file IO**
  ```c#
  public class Logger
    {

        private static Logger logger = new Logger();
        private string FilePath = null;
        private FileStream fs = null;
        private StreamWriter writer = null;
        private Logger() 
        {
            this.FilePath = ConfigurationManager.AppSettings["LogFilePath"].ToString();
        }

        public static Logger CurrentLogger { get { return logger; } }

        public void Log(string message)
        {
            try
            {
                if (File.Exists(this.FilePath))
                {
                    fs = new FileStream(FilePath, FileMode.Append, FileAccess.Write);

                }
                else
                {
                    fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write);

                }

                writer = new StreamWriter(fs);

                writer.WriteLine(string.Format("Log : @ {0} : Details : {1}", DateTime.Now, message));

                writer.Flush();
               
            }
            catch (Exception ex)
            {

                // EMail
            }
            finally
            {
                writer.Close();

                fs.Close();

                writer = null;
                fs = null;
            }
        }

    }


  ```    
   
  > **implement action and result filter using logger**
  ```c#
  public class CustomFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("{0}/{1} is about to execute",
                
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("{0}/{1}  executed",

                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("UI Processing is about to begin"));
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("UI Processing is done"));
        }
    }


  ```



  4.  **Exception Filter**
    -  used to  handle exception , using an attribute
       1. in .net to be able to show custom error page , need to add to Web.COnfig file
      ```c#

       // mode can be Off(no custom error)/On(allowed custom)/Remote(only on client machine custom error allowed)
        <customErrors mode="On"> </customErrors>

      ```
      1. need to add exception attribute on base class i.e HandleError ,as it can be inhertied in child classes
   
       ```c#
          [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
          [HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error")]
          [HandleError(ExceptionType = typeof(Exception), View = "Error")]
          public class -----
          {}

      ```
       - to get error message to page need to use
        ```c#
           @Model.Exception.Message
      ```
       
      
# Day 18
- topics to cover today
1. HTML Helper
2. Scaffolding
3. Validation
4. AntiForgery Token
5. Different Return Tyoes (eg JSON)
6. AJAX using JQuery

<hr> 

### strctly type casting in cshtml file 
 - it helps to get intellisense and, compile time allocate object, which is default dynamic
 ```c#
  @model demo1.Models.Emp

   <body> 

   <p> @Model.--- (Emp properties avaiable now, i.e intellisense)
```



1. **HTML Helper**
  - 1. Helper are used to generate UI elements in views
  - we can generate views directly from controller , using create view option on right click  
  - eg.  
    ```c#
   
    --- syntax

    @Html.HtmlTag(); 

    ---
    eg. 
    
     @Html.TextArea("Name"); //gives qa textbox with Name

    ``` 
  
 



2. **Scaffolding**
- 1. ASP.NET Scaffolding is a code generation framework for ASP.NET Web applications.
   -  Visual Studio 2013 includes pre-installed code generators for MVC and Web API projects. 
   -  You add scaffolding to your project when you want to quickly add code that interacts with data models. 
   -   Using scaffolding can reduce the amount of time to develop standard data operations in your project.

   -  we have created the views for Index, Create, Edit actions and also need to update the actions methods as well. 
   -  But ASP.Net MVC provides an easier way to create all these Views and action methods using scaffolding.

   - Scaffolding consists of 
     - page templates, entity page templates, field page templates, and filter templates. 
   - These templates are called **Scaffold templates** and allow you to quickly build a functional data-driven Website.

- 2. **Scaffolding Templates:**

   - 1. **Create:**
      -  It creates a View that helps in creating a new record for the Model. It automatically generates a label and input field for each property in the Model.

   - 2. **Delete:**
      -  It creates a list of records from the model collection along with the delete link with delete record.

   - 3. **Details:**
      -  It generates a view that displays the label and an input field of the each property of the Model in the MVC framework.

   - 4. **Edit:**
      -  It creates a View with a form that helps in editing the current Model.
        -  It also generates a form with label and field for each property of the model.

   - 5. **List:**
      -  It generally creates a View with the help of a HTML table that lists the Models from the Model Collection. It also generates a HTML table column for each property of the Model.


- 3. **Add a Scaffolded Item**
   - To add a scaffold, right-click on Controllers folder in the Solution Explorer and select Add → New Scaffolded Item.
!['how to add scaffold'](Scaffolding.png) 

- Select MVC 5 Controller with views, using Entity Framework in the middle pane and click ‘Add’ button, which will display the Add Controller dialog.

!['howtoaddscaffold2'](Addscafolding2.png)

<hr>

 - 2 .view creation at  method level by using add view option, can be created directly at controller level using scaffolding
     - 1. Index
 ```c#
    @model IEnumerable<demo1.Models.Emp>

@{
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/MasterLayout.cshtml";
}

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(model => model.No)
        </th>
        <th>
            @Html.DisplayNameFor(model => model.Name)
        </th>
        <th>
            @Html.DisplayNameFor(model => model.Address)
        </th>
        <th></th>
    </tr>

@foreach (var item in Model) {
    <tr>
        <td>
            @Html.DisplayFor(modelItem => item.No)
        </td>
        <td>
            @Html.DisplayFor(modelItem => item.Name)
        </td>
        <td>
            @Html.DisplayFor(modelItem => item.Address)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", new {  id=item.No }) |
            @Html.ActionLink("Delete", "Delete", new {  id=item.No  })
        </td>
    </tr>
}

</table>
 ``` 

   - 2.  Create
```c#
@model demo1.Models.Emp

@{
    ViewBag.Title = "Create";
    Layout = "~/Views/Shared/MasterLayout.cshtml";
}

<h2>Create</h2>


@using (Html.BeginForm()) 
{
    @Html.AntiForgeryToken()
    
    <div class="form-horizontal">
        <h4>Emp</h4>
        <hr />
        @Html.ValidationSummary(true, "", new { @class = "text-danger" })
        <div class="form-group">
            @Html.LabelFor(model => model.No, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.No, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.No, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(model => model.Name, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Name, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Name, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(model => model.Address, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Address, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Address, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Create" class="btn btn-default" />
            </div>
        </div>
    </div>
}

<div>
    @Html.ActionLink("Back to List", "Index")
</div>

<script src="~/Scripts/jquery-3.4.1.min.js"></script>
<script src="~/Scripts/jquery.validate.min.js"></script>
<script src="~/Scripts/jquery.validate.unobtrusive.min.js"></script>
```

  - 3. Edit page

```c#
@model demo1.Models.Emp

@{
    ViewBag.Title = "Edit";
    Layout = "~/Views/Shared/MasterLayout.cshtml";
}

<h2>Edit</h2>


@using (Html.BeginForm())
{
    @Html.AntiForgeryToken()
    
    <div class="form-horizontal">
        <h4>Emp</h4>
        <hr />
        @Html.ValidationSummary(true, "", new { @class = "text-danger" })
        <div class="form-group">
            @Html.LabelFor(model => model.No, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.No, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.No, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(model => model.Name, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Name, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Name, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(model => model.Address, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Address, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Address, "", new { @class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" class="btn btn-default" />
            </div>
        </div>
    </div>
}

<div>
    @Html.ActionLink("Back to List", "Index")
</div>

<script src="~/Scripts/jquery-3.4.1.min.js"></script>
<script src="~/Scripts/jquery.validate.min.js"></script>
<script src="~/Scripts/jquery.validate.unobtrusive.min.js"></script>



```


3. Validation
- need to use Data annotation for validation, so need to  include 
```c#
using System.ComponentModel.DataAnnotations;

```
- Validation on server side
  - persisting validation  (custom) on data model Emp
 ```c#
  [MetadataType(typeof(ExtraInfo_About_Emp))]
    public partial class Emp
    {

    }


    public class ExtraInfo_About_Emp
    {
        [Required(ErrorMessage = "No is must")]
        [Range(1, 100, ErrorMessage = "No must be between 1 to 100")]
        public int No { get; set; }

        [Required(ErrorMessage = "Name is must")]
        [DemoValidator(ErrorMessage = "1234 is not a valid Name")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Address is must")]
        public string Address { get; set; }
    }
}

 ```


4. AntiForgery Token
- validation of data, to avoid forgery attack , due to 
   - client click ads on website that ,makes changes(error) in db 
5. **Different Return Types on controller Methods i.e Result** 
 - all are under ActionResult abstract class
 - There are total 10 main types of result, ActionResult is main type and others are sub types of results 
 - we use these return types to return results from controller to view.
 - types of Results are: 
  1. **ViewResult (View):**
   -  This return type is used to return a webpage from an action method.
  2. **PartialviewResult (Partialview):**
   -  This return type is used to send a part of a view which will be rendered in another view.
  3. **RedirectResult (Redirect):**
   -  This return type is used to redirect to any other controller and action method depending on the URL.
  4. **RedirectToRouteResult (RedirectToAction, RedirectToRoute):**
   - This return type is used when we want to redirect to any other action method.
   - 
  5. **ContentResult (Content):** 
   - This return type is used to return HTTP content type like text/plain as the result of the action.
  6. **jsonResult (json):**
   -  This return type is used when we want to return a JSON message.
  7. **javascriptResult (javascript):** 
   - This return type is used to return JavaScript code that will run in browser.
  8. **FileResult (File):**
   -  This return type is used to send binary output in response.
  9.  **EmptyResult:**
   -  This return type is used to return nothing (void) in the result. 
  10. ActionResult:
    -  This return type is used to return all 9 subtypes in the result.
```c#
 public ActionResult Index(int? id)
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    Emp emp = new Emp() { No = 1, Name = "rajat", Address = "shimla" };
                    result =  View(emp);

                    break;

                case 2:
                    result = new JsonResult()
                    {
                        Data = dbObj.Emps.ToList(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    break;

                case 3:
                    // give result as js by creating js 
                    result = new JavaScriptResult() { Script = "alert('Hello from JS')"};

                    break;
                case 4:
                    result = new ContentResult() { ContentType = "text/plain", Content = "hello to content type" };

                    break;

                default:
                    break;
            }
          
            


            return result;
        }


```

6. **Asynchronous JavaScript and XML (AJAX) using JQuery**
  
  - use of ajax  as for data updation continuously
  1. **Using .ajax method** 
```c#
<script>
 function GetData() 
          {
        
                 $.ajax({
                url: "/Test/Index/2",
                contentType: "application/json",
                type: "GET",
                success: function (result) {
                    debugger;
                    $("#containerUL").html('');

                    result.map((item) => {
                        let liElement = "<li>" + item.No + " | " + item.Name + " | " + item.Address + " | " + "</li>";
                        $("#containerUL").append(liElement);
                    });
                    debugger;

                },
                error: function (error) {
                    debugger;
                    alert("Something is not right here!")
                }
            });
        }

        window.setInterval(GetData, 5000);
    </script>

```
  2. **Using XmlHttpRequest**
  ```c#
   <script>

        function GetData()
         {
            debugger;

            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/Test/Index/2");
            xhr.send();
            xhr.onreadystatechange = function () {
                debugger;
                if (xhr.readyState == 4)  //0,1,2,3,4
                {
                    var result = JSON.parse(xhr.responseText);
                    var container = document.getElementById("containerUL");
                    container.innerHTML = '';

                    result.map((item) => {
                        let liElement = "<li>" + item.No + " | " + item.Name + " | " + item.Address + " | " + "</li>";
                        container.innerHTML = container.innerHTML + liElement;
                    });
                }
            }
         }

        window.setInterval(GetData, 5000); //repeating at fixed interval
    </script>


   ```       


# Day19

### Service oriented Architecture 

!['serv-arch'](service-based-architecture.jpg)

1. how to deploy your logic(app) safely: 
2. needed service architecture because 
   - if one shares dll files one can rebuilt the app and can use it for unfair use
   - if one shares exe files ,one can rebulit the app, from MSIL 
   - for data communication between server and client 
    
   
  
       - diffrent types of  frameworks :  (to send data between web components)  
   -  1. **COM+** | TCP|BInary
       - **Component Object Model** is a binary-interface standard for software components introduced by Microsoft in 1993.
       -  It is used to enable inter-process communication object creation in a large range of programming languages
   -  2. **Remoting**     | TCP|BInary| .NET
       -  .NET Remoting is a mechanism that allows objects to interact with each other across application domains, whether application components are all on one computer or spread out across the entire world. 
       -  Remoting was designed in such a way that it hides the most difficult aspects like managing connections, marshaling data, and reading and writing XML and SOAP.
       -  Advantage
         - Remoting favors the runtime type system, and provides a more complex programming model with much more limited reach.
       - Remoting is allowing you to communicate between a client application and components in a binary format. 
       - It is more suitable as a high speed solution for binary communication between proprietary .NET components, usually over an internal network.  

   -  3. **Web Service** HTTP|SOAP|
     - 	A web service is a method of communication between two software units over the World Wide Web. Applications can access Web services through different data formats such as HTTP, XML, and SOAP, with no need to worry about how each Web service is implemented.
     - Extensible Markup Language (XML) is used to tag the data, Simple Object Access Protocol (SOAP) is used to transfer the data, Web Services Description Language (WSDL) is used for describing the services available and
        -  **Universal Description Discovery and Integration (UDDI)** is used for listing what services are available.
        -   - it is an XML-based registry for businesses worldwide to list themselves on the Internet. Its ultimate goal is to streamline online transactions by enabling companies to find one another on the Web and make their systems interoperable for e-commerce.
    - some are free  and some are paid.
    - these have extension as **wsdl** 
     - Web services do not provide the user with a GUI, instead it share business logic, data and processes through a programmatic interface across a network.
   - Advantage : 
      - Web services favor the XML Schema type system, and provide a simple programming model with broad cross-platform reach,  
      - If both your clients and components are inside the firewall, Web services will work fine.
   - Disadvantage
      -  However, all of your data travels through a Web server, which can slow performance.

   - 4. **WEB API**
       - ASP.NET Web API is a framework that makes it easy to build HTTP services that reach a broad range of clients, including browsers and mobile devices. 
       -   MVC and WCF team came to give Web API 
       -  ASP.NET Web API is an ideal platform for building RESTful applications on the .NET Framework.
       - When you're building APIs on the Web, there are several ways you can build APIs on the Web. 
       - These include HTTP/RPC, and what this means is using HTTP in Remote Procedure Call to call into things, like Methods, across the Web.
       - The verbs themselves are included in the APIs, like Get Customers, Insert Invoice, Delete Customer, and that each of these endpoints end up being a separate URI.
       - things to include 
    ```c#
    // namespace to be included
        need to include 
        using System.Net;
        using System.Data.Entity;
        using System.Data.Entity.Infrastructure;
        using System.Web.Http;
        using System.Web.Http.Description;

    //add to Global Config file 
     GlobalConfiguration.Configure(WebApiConfig.Register);

    ```
       -  here we can call methods like 
```c#
//call using the syntax
 routeTemplate: "api/{controller}/{id}",


```

  - 1. to implement web api with entity framework 
```c#
public class EmpsController : ApiController
    {
        private DemoDBEntities db = new DemoDBEntities();

        // GET: api/Emps
        public IQueryable<Emp> GetEmps()
        {
            return db.Emps;
        }

        // GET: api/Emps/5
        [ResponseType(typeof(Emp))]
        public IHttpActionResult GetEmp(int id)
        {
            Emp emp = db.Emps.Find(id);
            if (emp == null)
            {
                return NotFound();
            }

            return Ok(emp);
        }

        // PUT: api/Emps/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmp(int id, Emp emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != emp.No)
            {
                return BadRequest();
            }

            db.Entry(emp).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent); // no centent i.e 200 status code 
        }

        // POST: api/Emps
        [ResponseType(typeof(Emp))]
        public IHttpActionResult PostEmp(Emp emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Emps.Add(emp);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EmpExists(emp.No))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = emp.No }, emp);
        }

        // DELETE: api/Emps/5
        [ResponseType(typeof(Emp))]
        public IHttpActionResult DeleteEmp(int id)
        {
            Emp emp = db.Emps.Find(id);
            if (emp == null)
            {
                return NotFound();
            }

            db.Emps.Remove(emp);
            db.SaveChanges();

            return Ok(emp); /// ok gives type of datatype
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmpExists(int id)
        {
            return db.Emps.Count(e => e.No == id) > 0; // check EMp exist or not
        }
    }


```

 - 2. call to Api methods is givas as 
    1. GET
       -  api/Emps  -- for all  Emps
       -  api/Emps/5  --- for id emp 
    2. POST
       -  api/Emps
    3. PUT
       - api/Emps/5
    4. DELETE 
       - api/Emps/5



### Framework for building Service oriented application :
1. **Windows Communication Foundation (WCF)** 
 -   it is a framework for building service-oriented applications. 
   -   came in 2006 for web communication 
   -   here we have SVC file 
   -   user call to svc file / not asmx 


 -  Using WCF, you can send data as asynchronous messages from one service endpoint to another.
 -   A service endpoint can be part of a continuously available service hosted by IIS, or it can be a service hosted in an application. 
 -   An endpoint can be a client of a service that requests data from a service endpoint. 
 -   The messages can be as simple as a single character or word sent as XML, or as complex as a stream of binary data. 
 -   A few sample scenarios include:

    1. A secure service to process business transactions.

    2. A service that supplies current data to others, such as a traffic report or other monitoring service.

    3.  A chat service that allows two people to communicate or exchange data in real time.

    4. A dashboard application that polls one or more services for data and presents it in a logical presentation.


-  WCF makes the development of endpoints easier than ever. In summary,
-   WCF is designed to offer a manageable approach to creating Web services and Web service clients.
   
2. **Features of WCF**

  - WCF includes the following set of features. 

     1. Service Orientation

       -  One consequence of using WS standards is that WCF enables you to create service oriented applications. 
       -  Service-oriented architecture (SOA) is the reliance on Web services to send and receive data.
       -   The services have the general advantage of being loosely-coupled instead of hard-coded from one application to another.
       -    A loosely-coupled relationship implies that any client created on any platform can connect to any service as long as the essential contracts are met.

    2. Interoperability

       - WCF implements modern industry standards for Web service interoperability. 

    3. Multiple Message Patterns

       - Messages are exchanged in one of several patterns. The most common pattern is 
       - 1. **the request/reply pattern**, where one endpoint requests data from a second endpoint. The second endpoint replies. 
       - 2. **one-way message** in which a single endpoint sends a message without any expectation of a reply.
       - 3. **duplex exchange pattern** is a  more complex pattern  where two endpoints establish a connection and send data back and forth, similar to an instant messaging program.

    4. Service Metadata

       - WCF supports publishing service metadata using formats specified in industry standards such as WSDL, XML Schema and WS-Policy.
       -  This metadata can be used to automatically generate and configure clients for accessing WCF services. 
       -  Metadata can be published over HTTP and HTTPS or using the Web Service Metadata Exchange standard. 
  
    5. Data Contracts

       -  Because WCF is built using the .NET Framework, it also includes code-friendly methods of supplying the contracts you want to enforce.
       -   One of the universal types of contracts is the data contract. 
       -   WCF includes a comprehensive system for working with data in this easy manner.
       -    Once you have created the classes that represent data, your service automatically generates the metadata that allows clients to comply with the data types you have designed. 

   6. Security

       -Messages can be encrypted to protect privacy and you can require users to authenticate themselves before being allowed to receive messages.
        Security can be implemented using well-known standards such as SSL or WS-SecureConversation. 

    7. Multiple Transports and Encodings

       - Messages can be sent on any of several built-in transport protocols and encodings. 
       The most common protocol and encoding is to send text encoded SOAP messages using the HTTP for use on the World Wide Web. 
       - Alternatively, WCF allows you to send messages over TCP, named pipes, or MSMQ.
       -  These messages can be encoded as text or using an optimized binary format. 
       -   you can create your own custom transport or encoding. 
   
    8. Reliable and Queued Messages

       - WCF supports reliable message exchange using reliable sessions implemented over WS-Reliable Messaging and using MSMQ.

    9. Durable Messages

       - A durable message is one that is never lost due to a disruption in the communication.
       -  The messages in a durable message pattern are always saved to a database. 
       -  If a disruption occurs, the database allows you to resume the message exchange when the connection is restored.
       -   You can also create a durable message using the Windows Workflow Foundation (WF).

    10. Transactions

       - WCF also supports transactions using one of three transaction models:
       - 1. WS-AtomicTransactions, the APIs in the System.
       - 2. Transactions namespace, and 
       - 3. Microsoft Distributed Transaction Coordinator. 

    11. AJAX and REST Support

       - REST is an example of an evolving Web 2.0 technology. 
       - WCF can be configured to process "plain" XML data that is not wrapped in a SOAP envelope. 
       - WCF can also be extended to support specific XML formats, such as ATOM (a popular RSS standard), and even non-XML formats, such as JavaScript Object Notation (JSON).

    12. Extensibility

       - The WCF architecture has a number of extensibility points.
       -  If extra capability is required, there are a number of entry points that allow you to customize the behavior of a service.
- e.g 

```c#
using Consumer2.ServiceReference1;
namespace Consumer2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataWebServiceSoapClient proxyobj = new DataWebServiceSoapClient();

       int result =     proxyobj.Add(10, 20);

          int r2 =   proxyobj.Sub(20,10);
            Console.WriteLine(result);
            Console.WriteLine(r2);
        }
    }
}

```
   
  
      

### WPF Microsoft's UI framework to create applications 

  
1. **Windows Presentation Foundation (WPF)**
-   it is a UI framework that creates desktop client applications for Windows. 
  - The WPF development platform supports a broad set of application development features, including an application model, controls, graphics, and data binding.
  -  WPF uses Extensible Application Markup Language (XAML) to provide a declarative model for application programming.
  


2. There are **two implementations of :**

  - 1. The open-source implementation hosted on GitHub. This version runs on .NET Core 3.0. The WPF Visual Designer for XAML requires, at a minimum, Visual Studio 2019 version 16.3.

   - 2. The .NET Framework implementation that's supported by Visual Studio 2019 and Visual Studio 2017.


3. **XAML**

- XAML is a declarative XML-based language that WPF uses for things such as defining resources or UI elements. 
- Elements defined in XAML represent the instantiation of objects from an assembly.

- This example is intended to give you an idea of how XAML represents an object, where Button is the type and Content is a property.
```xml
<StackPanel>
    <Button Content="Click Me!" />
</StackPanel>
```

 - **XAML extensions**

   - XAML provides syntax for markup extensions. Markup extensions can be used to provide values for properties in attribute form, property-element form, or both.
   - A markup extension is defined with opening and closing curly braces { }. 
   - The type of markup extension is then identified by the string token immediately following the opening curly brace.
``xml

<StackPanel>
    <Button Content="{MarkupType}" />
</StackPanel>
```

   - **Data binding** is the process that establishes a connection between the application UI and business logic. 
   - WPF provides different markup extensions for XAML such as {Binding} for data binding.
   -  Collectively, these services are referred to as the WPF property system. A property that is backed by the WPF property system is known as a **dependency property.**
  
    - The purpose of dependency properties is to provide a way to compute the value of a property based on the value of other inputs. 
These other inputs might include system properties such as themes and user preferences, or just-in-time property from data binding and animations.

   -  A dependency property can be implemented to provide validation, default values, and callbacks that monitor changes to other properties. 

- **Dependency object**

    - This type defines the base class that can register and own a dependency property. 
    The GetValue and SetValue methods provide the backing implementation of the dependency property for the dependency object instance.

The following example shows a dependency object that defines a single dependency property identifier named ValueProperty. The dependency property is created with the Value .NET property.
```C#

   public class TextField: DependencyObject
   {
    public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register("Value", typeof(string), typeof(TextField), new PropertyMetadata(""));

    public string Value
    {
        get { return (string)GetValue(ValueProperty); }
        set { SetValue(ValueProperty, value); }
    }
    }
```
- Data binding can be configured in XAML through the {Binding} markup extension.
 - The following example demonstrates binding to a data object's ButtonText property. If that binding fails, the value of Click Me! is used.
```xml

<StackPanel>
    <Button Content="{Binding ButtonText, FallbackValue='Click Me!'}" />
</StackPanel>
```
   

4. **diff between MVC, WCF and Web API**
  -  WCF services use the SOAP protocol while HTTP never use SOAP protocol. 
   - That’s why WebAPI services are lightweight since SOAP is not used.
   -  MVC framework is used for developing applications which have User Interface. For that, views can be used for building a user interface
   - WebAPI is used for developing HTTP services. Other apps can also be called the WebAPI methods to fetch that data.   


       
4. **Deployment/Publishing of web site** 
 - we can go to solution explorer  ---> (right click) project --> select Publish
 - it will generate a publish , that we can directly upload to a website ,or to a folder
 - now this pubslished folder contains all MVC, and a bin folder with compiled logic , so it cant be copied
 - for Database we  used, we need to coonect
  -   to ms sql server management,
     - here we can generate sql script file of database by
     -  ---> Database--->(right click) generate script ---> Database script (schema + data) --- new .sql file
 - further 

5. **(WPF) WIndows Presentation Foundation**
- Windows Presentation Foundation (WPF) is a free and open-source graphical subsystem (similar to WinForms) originally developed by Microsoft for rendering user interfaces in Windows-based applications. 
- WPF, previously known as "Avalon", was initially released as part of .NET Framework 3.0 in 2006. WPF uses DirectX and attempts to provide a consistent programming model for building applications. It separates the user interface from business logic, and resembles similar XML-oriented object models, such as those implemented in XUL and SVG.[1] 
 - UI code will be XAML 
 - having XAML.CS contain C# code
 - now code converted to Assembly by CLR, containing in Resources  called Binary Application Markup Language  (BAML) 
 - now we have XAML Compiler for XAML
 - now CLR comes to Resources it comes to BAML ,and goes to XAML Runtime  
 -  **Extensible Application Markup Language** (XAML)
    - UI based(containing) XMl with XSD (given by Windows)  is called XAML  
  - all windows based app are 
     - made in .NET
     - UI constructd in WPF
     - code in C#  

 

 - to run a WPF application 
 - select in project , WPF, it creates two files  
 - here  we  have 
 -  1. MianWindow.xaml (UI )
 ```c#
 <Window x:Class="Demo2_WpfApp.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:Demo2_WpfApp"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <StackPanel>
        <Slider x:Name="sld" Height="80" Width="300" Minimum="0" Maximum="360" ValueChanged="sld_ValueChanged"> </Slider>
        <Button Name="btn" Height="80" Width="200" Click="btn_Click">

            <Button.Content>
                <TextBox x:Name="txt" Height="50" Width="100" />
            </Button.Content>

            <Button.RenderTransform>
                <RotateTransform Angle="{Binding ElementName=sld, Path=Value}"></RotateTransform>
            </Button.RenderTransform>
        </Button>
        
    </StackPanel>
</Window>




 ```

-   2. MainWindow.xaml.cs (Logic)
```c#
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Windows;
  using System.Windows.Controls;
  using System.Windows.Data;
  using System.Windows.Documents;
  using System.Windows.Input;
  using System.Windows.Media;
  using System.Windows.Media.Imaging;
  using System.Windows.Navigation;
  using System.Windows.Shapes;
  namespace demoWPF
    {
      /// <summary>
      /// Interaction logic for   MainWindow.xaml
      /// </summary>
      public partial class MainWindow :   Window
      {
          public MainWindow()
          {
            InitializeComponent();
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello" + txt.Text);
        }
    }
   }
```
### Threading 

!['thread'](threading.jpg)

- thread is a OS level concept
- thread  has delegate pointing to a function 
- cpu process one thread at a time
- .NET is a single threaded application 
- so all application created by default will be Single threaded

### now we have **TASK**  replacing threading 
- 3 task = 1 thread 
- create Task t1 = new Task();
- n number of task make Global Task Queue 
- now it goes through (Scheduler ) 
- now for multi processor pc ,i.e CPU1 ,CPU2, CPU3 
- now scheduler makes queue of tasks for each processor 
- and createas a Thread(Task) Pool 
- now it can improve performance by
   - by shifting tasks between cpu(processor)
   - for multiple threading 
    - we have multiple Parallel Loops / methods  we can use 
    - e.g  
     - 1. use  Parallel.ForEach Loop 
     - this method creates task for each var object in objectCollection 
     - it is faster as running on mutliple cpu
    ```c#

    Parallel.ForEach(var item in itemlist) {}
  
    ```

    - 2. methoad AsParallel() 
    ```c#

    var result = from emp in emps.AsParrallel();
  
    ```
 ### how to improve performance i .NET?

 1. Expression Tree 
 2. LINQ
 3. Lambda Expression 
 4.  using Task 
 5.  using Parralel methods
 6.   


# Contact details Sir 
> Mahesh@bonaventuresystems.com
> 9922412058
